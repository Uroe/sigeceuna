module cr.ac.una.sigeceuna {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;
    requires java.ws.rs;
    requires java.xml.bind;
    requires java.sql;
    requires java.base;
    requires com.jfoenix;
    requires cr.ac.una.gestorseguridad;
    requires org.controlsfx.controls;
    requires poi;
    requires poi.ooxml;
    requires poi.ooxml.schemas;
    requires castor.xml;
    requires castor.core;
    requires jakarta.inject;
    requires jasperreports;
    requires java.desktop;

    opens cr.ac.una.sigeceuna.controller to javafx.fxml, javafx.controls, com.jfoenix, org.controlsfx.controls;
    exports cr.ac.una.sigeceuna to javafx.graphics;
    exports cr.ac.una.sigeceuna.model;

}
