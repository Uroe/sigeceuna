package cr.ac.una.sigeceuna.util;

import java.awt.Dimension;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javax.swing.JComponent;
import javax.swing.JFrame;


/**
 *
 * @author Lesmi
 */
public class VisorReportes extends JFrame {

    JFrame frame = new JFrame();

    public VisorReportes() {
    }

    public void visor(JComponent jv) {
        try {
            frame.add(jv);
            frame.setSize(new Dimension(820, 600));
            frame.setLocationRelativeTo(null);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
        } catch (Exception e) {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Error", new Stage(), "Ocurrio un error"
                    + "abriendo el reporte");
        }
    }
}
