/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.util;

import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.GestionDto;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GenerarExcel {

    File file = new File("");
    GestionDto gestion;

    public GenerarExcel(GestionDto gestion) {
        this.gestion = gestion;
    }

    public void crearExcel() {

        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        UsuarioDto usuario = gestor.getUsuario(gestion.getGesSolicitante());
        gestion.setNomSolicitante(usuario.getUsuNombre());
        usuario = gestor.getUsuario(gestion.getGesPasignada());
        gestion.setNomPasignada(usuario.getUsuNombre());

        String fileName = "datos.xlsx";
        String filePath = file.getAbsolutePath() + fileName;
        //Seteando el nombre de la hoja donde agregaremos los items
        String hoja = "Hoja1";

        //Creando objeto libro de Excel
        XSSFWorkbook book = new XSSFWorkbook();
        XSSFSheet hoja1 = book.createSheet(hoja);

        //Cabecera de la hoja de excel
        String[] header = new String[]{"Id", "Asunto", "Estado", "Fecha de creacion", "Fecha de vencimiento", "Solicitante",
            "Asignado a ", "Area", "Actividad", "Subactividad"};

        String[][] document = new String[][]{
            {gestion.getGesId().toString(), gestion.getGesAsunto(),
                gestion.getGesEstado(), gestion.getGesFcreacion().toString(),
                gestion.getGesFvencimiento().toString(),
                gestion.getNomSolicitante(), gestion.getNomPasignada(),
                gestion.getNomArea(),
                gestion.getActividad().getNombre(),
                gestion.getSubActividad().getNombre()}
        };

        //Aplicando estilo color negrita a los encabezados
        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);//Seteando fuente negrita al encabezado del archivo excel
        style.setFont(font);

        //Generando el contenido del archivo de Excel
        for (int i = 0; i <= document.length; i++) {
            XSSFRow row = hoja1.createRow(i);//se crea las filas
            for (int j = 0; j < header.length; j++) {
                if (i == 0) {//Recorriendo cabecera
                    XSSFCell cell = row.createCell(j);//Creando la celda de la cabecera en conjunto con la posición
                    cell.setCellStyle(style); //Añadiendo estilo a la celda creada anteriormente
                    cell.setCellValue(header[j]);//Añadiendo el contenido de nuestra lista de productos
                } else {//para el contenido
                    XSSFCell cell = row.createCell(j);//Creando celda para el contenido del producto
                    cell.setCellValue(document[i - 1][j]); //Añadiendo el contenido
                }
            }
        }
        File excelFile;
        excelFile = new File(filePath); // Referenciando a la ruta y el archivo Excel a crear
        try (FileOutputStream fileOuS = new FileOutputStream(excelFile)) {
            if (excelFile.exists()) { // Si el archivo existe lo eliminaremos
                excelFile.delete();
                System.out.println("Archivo eliminado.!");
            }
            book.write(fileOuS);
            fileOuS.flush();
            fileOuS.close();
            System.out.println("Archivo Creado.!");
            try {
                Runtime.getRuntime().exec("cmd /d start " + filePath);
                Runtime.getRuntime().exec("cmd /c start " + filePath);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
