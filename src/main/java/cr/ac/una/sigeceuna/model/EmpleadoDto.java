/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.model;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author JosueNG
 */
public class EmpleadoDto {
    
    public SimpleStringProperty id;
    public Boolean modificado;
    List<AreaDto> areas;

    public EmpleadoDto() {
        this.id = new SimpleStringProperty();
        this.modificado = false;
        areas = new ArrayList<>();
    }
    
    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }
    
    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }
    
    public List<AreaDto> getAreas() {
        return areas;
    }

    public void setAreas(List<AreaDto> areas) {
        this.areas = areas;
    }

    @Override
    public String toString() {
        return "EmpleadoDto{" + "id=" + id + '}';
    }
}
