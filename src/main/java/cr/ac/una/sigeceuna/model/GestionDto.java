/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Lesmi
 */
public class GestionDto {
    
    public SimpleStringProperty gesId;
    public SimpleStringProperty gesSolicitante;
    public SimpleStringProperty gesPasignada;
    public ObjectProperty<LocalDate> gesFcreacion;
    public ObjectProperty<LocalDate> gesFvencimiento;
    public ObjectProperty<LocalDate> gesFsolucion;
    public SimpleStringProperty gesAsunto;
    public SimpleStringProperty gesDescripcion;
    public ObjectProperty<String> gesEstado;
    /*
    Para usarlos en los tableview
     */
    public SimpleStringProperty nomSolicitante;
    public SimpleStringProperty nomPasignada;
    public SimpleStringProperty nomArea;
    public SimpleStringProperty nomActividad;
    public SimpleStringProperty nomSubactividad;
    public SimpleStringProperty diasRestantes;
    public SimpleStringProperty nomAreaSoli;
    public SimpleStringProperty nomAreaPer;
    /*
    Para usarlos como string
     */
    public SimpleStringProperty fechaCreacion;
    public SimpleStringProperty fechaVencimiento;
    /*
     */
    public SimpleStringProperty gesVersion;
    List<DocumentoDto> documentos;
    List<SeguimientoDto> seguimientos;
    List<AprobadorDto> aprobadores;
    List<AprobadorDto> aprobadoresElim;
    private ActividadDto actividad;
    private SubActividadDto subActividad;
    
    private Boolean modificado;
    
    public GestionDto() {
        this.modificado = false;
        this.gesId = new SimpleStringProperty();
        this.gesSolicitante = new SimpleStringProperty();
        this.gesPasignada = new SimpleStringProperty();
        this.gesAsunto = new SimpleStringProperty();
        this.gesDescripcion = new SimpleStringProperty();
        this.gesEstado = new SimpleObjectProperty("P");
        this.gesFcreacion = new SimpleObjectProperty<>();
        this.gesFvencimiento = new SimpleObjectProperty<>();
        this.gesFsolucion = new SimpleObjectProperty<>();
        this.nomSolicitante = new SimpleStringProperty();
        this.nomPasignada = new SimpleStringProperty();
        this.nomArea = new SimpleStringProperty();
        this.nomActividad = new SimpleStringProperty();
        this.nomSubactividad = new SimpleStringProperty();
        this.fechaCreacion = new SimpleStringProperty();
        this.fechaVencimiento = new SimpleStringProperty();
        this.diasRestantes = new SimpleStringProperty();
        this.gesVersion = new SimpleStringProperty();
        this.nomAreaSoli = new SimpleStringProperty();
        this.nomAreaPer = new SimpleStringProperty();
        aprobadores = new ArrayList<>();
        seguimientos = new ArrayList<>();
        documentos = new ArrayList<>();
        actividad = new ActividadDto();
        subActividad = new SubActividadDto();
        aprobadoresElim = new ArrayList<>();
    }
    
    public String getNomAreaSoli() {
        return nomAreaSoli.get();
    }
    
    public void setNomAreaSoli(String nomAreaSoli) {
        this.nomAreaSoli.set(nomAreaSoli);
    }
    
    public String getNomAreaPer() {
        return nomAreaPer.get();
    }
    
    public void setNomAreaPer(String nomAreaPer) {
        this.nomAreaPer.set(nomAreaPer);
    }
    
    public String getDiasRestantes() {
        return diasRestantes.get();
    }
    
    public void setDiasRestantes(String diasRestantes) {
        this.diasRestantes.set(diasRestantes);
    }
    
    public String getFechaCreacion() {
        return fechaCreacion.get();
    }
    
    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion.set(fechaCreacion);
    }
    
    public String getFechaVencimiento() {
        return fechaVencimiento.get();
    }
    
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento.set(fechaVencimiento);
    }
    
    public String getNomArea() {
        return nomArea.get();
    }
    
    public void setNomArea(String nomArea) {
        this.nomArea.set(nomArea);
    }
    
    public String getNomActividad() {
        return nomActividad.get();
    }
    
    public void setNomActividad(String nomActividad) {
        this.nomActividad.set(nomActividad);
    }
    
    public String getNomSubactividad() {
        return nomSubactividad.get();
    }
    
    public void setNomSubactividad(String nomSubactividad) {
        this.nomSubactividad.set(nomSubactividad);
    }
    
    public String getNomSolicitante() {
        return nomSolicitante.get();
    }
    
    public void setNomSolicitante(String nomSolicitante) {
        this.nomSolicitante.set(nomSolicitante);
    }
    
    public String getNomPasignada() {
        return nomPasignada.get();
    }
    
    public void setNomPasignada(String nomPasignada) {
        this.nomPasignada.set(nomPasignada);
    }
    
    public Long getGesId() {
        if (gesId.get() != null && !gesId.get().isEmpty()) {
            return Long.valueOf(gesId.get());
        } else {
            return null;
        }
    }
    
    public void setGesId(Long gesId) {
        this.gesId.set(gesId.toString());
    }
    
    public Long getGesSolicitante() {
        if (gesSolicitante.get() != null && !gesSolicitante.get().isEmpty()) {
            return Long.valueOf(gesSolicitante.get());
        } else {
            return null;
        }
    }
    
    public void setGesSolicitante(Long gesSolicitante) {
        this.gesSolicitante.set(gesSolicitante.toString());
    }
    
    public Long getGesPasignada() {
        if (gesPasignada.get() != null && !gesPasignada.get().isEmpty()) {
            return Long.valueOf(gesPasignada.get());
        } else {
            return null;
        }
    }
    
    public void setGesPasignada(Long gesPasignada) {
        this.gesPasignada.set(gesPasignada.toString());
    }
    
    public LocalDate getGesFcreacion() {
        return gesFcreacion.get();
    }
    
    public void setGesFcreacion(LocalDate gesFcreacion) {
        this.gesFcreacion.set(gesFcreacion);
    }
    
    public LocalDate getGesFvencimiento() {
        return gesFvencimiento.get();
    }
    
    public void setGesFvencimiento(LocalDate gesFvencimiento) {
        this.gesFvencimiento.set(gesFvencimiento);
    }
    
    public LocalDate getGesFsolucion() {
        return gesFsolucion.get();
    }
    
    public void setGesFsolucion(LocalDate gesFsolucion) {
        this.gesFsolucion.set(gesFsolucion);
    }
    
    public String getGesAsunto() {
        return gesAsunto.get();
    }
    
    public void setGesAsunto(String gesAsunto) {
        this.gesAsunto.set(gesAsunto);
    }
    
    public String getGesDescripcion() {
        return gesDescripcion.get();
    }
    
    public void setGesDescripcion(String gesDescripcion) {
        this.gesDescripcion.set(gesDescripcion);
    }
    
    public String getGesEstado() {
        return gesEstado.get();
    }
    
    public void setGesEstado(String gesEstado) {
        this.gesEstado.set(gesEstado);
    }
    
    public List<DocumentoDto> getDocumentos() {
        return documentos;
    }
    
    public void setDocumentos(List<DocumentoDto> documentos) {
        this.documentos = documentos;
    }
    
    public List<SeguimientoDto> getSeguimientos() {
        return seguimientos;
    }
    
    public void setSeguimientos(List<SeguimientoDto> seguimientos) {
        this.seguimientos = seguimientos;
    }
    
    public List<AprobadorDto> getAprobadores() {
        return aprobadores;
    }
    
    public void setAprobadores(List<AprobadorDto> aprobadores) {
        this.aprobadores = aprobadores;
    }
    
    public ActividadDto getActividad() {
        return actividad;
    }
    
    public void setActividad(ActividadDto actividad) {
        this.actividad = actividad;
    }
    
    public SubActividadDto getSubActividad() {
        return subActividad;
    }
    
    public void setSubActividad(SubActividadDto subActividad) {
        this.subActividad = subActividad;
    }
    
    public Boolean getModificado() {
        return modificado;
    }
    
    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }
    
    public Long getGesVersion() {
        if (gesVersion.get() != null && !gesVersion.get().isEmpty()) {
            return Long.valueOf(gesVersion.get());
        } else {
            return null;
        }
    }
    
    public void setGesVersion(Long gesVersion) {
        this.gesVersion.set(gesVersion.toString());
    }
    
    public List<AprobadorDto> getAprobadoresElim() {
        return aprobadoresElim;
    }
    
    public void setAprobadoresElim(List<AprobadorDto> aprobadoresElim) {
        this.aprobadoresElim = aprobadoresElim;
    }
    
}
