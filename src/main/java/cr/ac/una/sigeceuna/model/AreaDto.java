/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.model;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author JosueNG
 */
public class AreaDto {

    public SimpleStringProperty id;
    public SimpleStringProperty nombre;
    public SimpleBooleanProperty estado;
    public byte[] logo;
    public Boolean modificado;
    List<EmpleadoDto> empleados;
    List<ActividadDto> actividades;
    List<EmpleadoDto> empleadosEliminados;

    public AreaDto() {
        this.id = new SimpleStringProperty();
        this.nombre = new SimpleStringProperty();
        this.estado = new SimpleBooleanProperty(true);
        this.logo = new byte[100];
        this.modificado = false;
        empleados = new ArrayList<>();
        actividades = new ArrayList<>();
        empleadosEliminados = new ArrayList<>();
    }

    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getEstado() {
        return estado.getValue() ? "A" : "I";
    }

    public void setEstado(String estado) {
        this.estado.setValue(estado.equalsIgnoreCase("A"));
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public List<EmpleadoDto> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<EmpleadoDto> empleados) {
        this.empleados = empleados;
    }

    public List<ActividadDto> getActividades() {
        return actividades;
    }

    public void setActividades(List<ActividadDto> actividades) {
        this.actividades = actividades;
    }

    public List<EmpleadoDto> getEmpleadosEliminados() {
        return empleadosEliminados;
    }

    public void setEmpleadosEliminados(List<EmpleadoDto> empleadosEliminados) {
        this.empleadosEliminados = empleadosEliminados;
    }

    @Override
    public String toString() {
        return "AreaDto{" + "id=" + id + ", nombre=" + nombre + ", estado=" + estado + ", logo=" + logo + '}';
    }
}
