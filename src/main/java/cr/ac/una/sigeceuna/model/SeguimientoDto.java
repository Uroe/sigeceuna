/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.model;

import java.time.LocalDate;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Ale
 */
public class SeguimientoDto {
    
    public SimpleStringProperty segId;
    public ObjectProperty<LocalDate> segFseguimiento;
    public SimpleStringProperty segIdusuario;
    public SimpleStringProperty segDetalle;
    
    private GestionDto gestion;
    
    public Boolean modificado;

    public SeguimientoDto() {
        this.modificado = false;
        this.segId = new SimpleStringProperty();
        this.segFseguimiento = new SimpleObjectProperty<>();
        this.segIdusuario = new SimpleStringProperty();
        this.segDetalle = new SimpleStringProperty();
        gestion = new GestionDto();
    }

    public Long getSegId() {
        if (segId.get() != null && !segId.get().isEmpty()) {
            return Long.valueOf(segId.get());
        } else {
            return null;
        }
    }

    public void setSegId(Long segId) {
        this.segId.set(segId.toString());
    }

    public LocalDate getSegFseguimiento() {
        return segFseguimiento.get();
    }

    public void setSegFseguimiento(LocalDate segFseguimiento) {
        this.segFseguimiento.set(segFseguimiento);
    }

    public Long getSegIdusuario() {
        if (segIdusuario.get() != null && !segIdusuario.get().isEmpty()) {
            return Long.valueOf(segIdusuario.get());
        } else {
            return null;
        }
    }

    public void setSegIdusuario(Long segIdusuario) {
        this.segIdusuario.set(segIdusuario.toString());
    }

    public String getSegDetalle() {
        return segDetalle.get();
    }

    public void setSegDetalle(String segDetalle) {
        this.segDetalle.set(segDetalle);
    }

    public GestionDto getGestion() {
        return gestion;
    }

    public void setGestion(GestionDto gestion) {
        this.gestion = gestion;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }
    
}
