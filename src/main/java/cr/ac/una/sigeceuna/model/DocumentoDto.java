/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.model;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Ale
 */
public class DocumentoDto {

    public SimpleStringProperty docId;
    public byte[] docArchivo;
    private GestionDto gestion;
    public Boolean modificado;

    public DocumentoDto() {
        this.modificado = false;
        this.docId = new SimpleStringProperty();
        this.docArchivo = new byte[100];
        this.gestion = new GestionDto();
    }

    public Long getDocId() {
        if (docId.get() != null && !docId.get().isEmpty()) {
            return Long.valueOf(docId.get());
        } else {
            return null;
        }
    }

    public void setDocId(Long docId) {
        this.docId.set(docId.toString());
    }

    public byte[] getDocArchivo() {
        return docArchivo;
    }

    public void setDocArchivo(byte[] docArchivo) {
        this.docArchivo = docArchivo;
    }

    public GestionDto getGestion() {
        return gestion;
    }

    public void setGestion(GestionDto gestion) {
        this.gestion = gestion;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public String toString() {
        return "DocumentoDto{" + "docId=" + docId + ", docUrl=" + docArchivo + ", gestion=" + gestion + ", modificado=" + modificado + '}';
    }

}
