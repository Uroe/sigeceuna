/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.model;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Ale
 */
public class AprobadorDto {
    
    public SimpleStringProperty id;
    public SimpleStringProperty comentario;
    public ObjectProperty<String> aprobacion;
    List<GestionDto> gestiones;
    
    private Boolean modificado;

    public AprobadorDto() {
        this.modificado = false;
        this.id = new SimpleStringProperty();
        this.comentario = new SimpleStringProperty();
        this.aprobacion = new SimpleObjectProperty("E");
        this.gestiones = new ArrayList<>();
    }

    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public String getComentario() {
        return comentario.get();
    }

    public void setComentario(String comentario) {
        this.comentario.set(comentario);
    }

    public String getAprobacion() {
        return aprobacion.get();
    }

    public void setAprobacion(String aprobacion) {
        this.aprobacion.set(aprobacion);
    }

    public List<GestionDto> getGestiones() {
        return gestiones;
    }

    public void setGestiones(List<GestionDto> gestiones) {
        this.gestiones = gestiones;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

}
