/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.model;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Lesmi
 */
public class ActividadDto {

    public SimpleStringProperty id;
    public SimpleStringProperty nombre;
    public SimpleStringProperty prioridad;
    public Boolean modificado;
    List<GestionDto> gestiones;
    List<SubActividadDto> subactividades;
    AreaDto area;

    public ActividadDto() {
        this.modificado = false;
        id = new SimpleStringProperty();
        nombre = new SimpleStringProperty();
        prioridad = new SimpleStringProperty();
        gestiones = new ArrayList<>();
        subactividades = new ArrayList<>();
        area = new AreaDto();
    }

    public Long getId() {
        if (id.get() != null && !id.get().isEmpty()) {
            return Long.valueOf(id.get());
        } else {
            return null;
        }
    }

    public void setId(Long id) {
        this.id.set(id.toString());
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public Long getPrioridad() {
        if (prioridad.get() != null && !prioridad.get().isEmpty()) {
            return Long.valueOf(prioridad.get());
        } else {
            return null;
        }
    }

    public List<GestionDto> getGestiones() {
        return gestiones;
    }

    public void setGestiones(List<GestionDto> gestiones) {
        this.gestiones = gestiones;
    }

    public void setPrioridad(Long prioridad) {
        this.prioridad.set(prioridad.toString());
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public List<SubActividadDto> getSubactividades() {
        return subactividades;
    }

    public void setSubactividades(List<SubActividadDto> subactividades) {
        this.subactividades = subactividades;
    }

    public AreaDto getArea() {
        return area;
    }

    public void setArea(AreaDto area) {
        this.area = area;
    }

}
