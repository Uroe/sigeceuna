/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.service;

import cr.ac.una.sigeceuna.model.SubActividadDto;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.Request;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author JosueNG
 */
public class SubActividadService {
    
    public Respuesta getSubActividad(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "SubActividadController/subActividad", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            SubActividadDto subActividadDto = (SubActividadDto) request.readEntity(SubActividadDto.class);
            return new Respuesta(true, "", "", "SubActividad", subActividadDto);

        } catch (Exception ex) {
            Logger.getLogger(SubActividadService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar la subActividad.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar la subActividad.", "getSubActividad " + ex.getMessage());
        }
    }

    public Respuesta cambiarPrioridad(Long id, Long prioridad) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            parametros.put("prioridad", prioridad);
            Request request = new Request(AppContext.getInstance().get("resturl") + "SubActividadController/subActividad", "/{id}/{prioridad}", parametros);
            request.put(id);

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            SubActividadDto subActividadDto = (SubActividadDto) request.readEntity(SubActividadDto.class);
            return new Respuesta(true, "", "", "SubActividad", subActividadDto);

        } catch (Exception ex) {
            Logger.getLogger(SubActividadService.class.getName()).log(Level.SEVERE, "Ocurrio un error al cambiar la prioridad.", ex);
            return new Respuesta(false, "Ocurrio un error al cambiar la prioridad.", "cambiarPrioridad " + ex.getMessage());
        }
    }

    public Respuesta getSubActividades() {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "SubActividadController/subActividades");
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<SubActividadDto> subActividades = (List<SubActividadDto>) request.readEntity(new GenericType<List<SubActividadDto>>() {
            });
            return new Respuesta(true, "", "", "SubActividades", subActividades);
        } catch (Exception ex) {
            Logger.getLogger(SubActividadService.class.getName()).log(Level.SEVERE, "Error obteniendo las subActividades.", ex);
            return new Respuesta(false, "Error obteniendo las subActividades.", "getSubActividades " + ex.getMessage());
        }
    }

    public Respuesta guardarSubActividad(SubActividadDto subActividad) {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "SubActividadController/subActividad");
            request.post(subActividad);
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            SubActividadDto subActividadDto = (SubActividadDto) request.readEntity(SubActividadDto.class);
            return new Respuesta(true, "", "", "SubActividad", subActividadDto);

        } catch (Exception ex) {
            Logger.getLogger(SubActividadService.class.getName()).log(Level.SEVERE, "Ocurrio un error al guardar la subActividad.", ex);
            return new Respuesta(false, "Ocurrio un error al guardar la subActividad.", "guardarSubActividad " + ex.getMessage());
        }
    }

    public Respuesta eliminarSubActividad(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "SubActividadController/subActividad", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(SubActividadService.class.getName()).log(Level.SEVERE, "Ocurrio un error al eliminar la subActividad.", ex);
            return new Respuesta(false, "Ocurrio un error al eliminar la subActividad.", "eliminarSubActividad " + ex.getMessage());
        }
    }
}
