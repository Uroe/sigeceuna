/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.service;

import cr.ac.una.sigeceuna.model.DocumentoDto;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.Request;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lesmi
 */
public class DocumentoService {

    public Respuesta getDocumento(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "DocumentoController/documento", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            DocumentoDto documentodto = (DocumentoDto) request.readEntity(DocumentoDto.class);
            return new Respuesta(true, "", "", "Documento", documentodto);

        } catch (Exception ex) {
            Logger.getLogger(DocumentoService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar el documento.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar el documento.", "getDocumento " + ex.getMessage());
        }
    }

    public Respuesta guardarDocumento(DocumentoDto documento) {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "DocumentoController/documento");
            request.post(documento);
            
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            DocumentoDto documentoDto = (DocumentoDto) request.readEntity(DocumentoDto.class);
            return new Respuesta(true, "", "", "Documento", documentoDto);

        } catch (Exception ex) {
            Logger.getLogger(DocumentoService.class.getName()).log(Level.SEVERE, "Ocurrio un error al guardar el documento.", ex);
            return new Respuesta(false, "Ocurrio un error al guardar el documento.", "guardarDocumento" + ex.getMessage());
        }
    }

    public Respuesta eliminarDocumento(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "DocumentoController/documento", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(DocumentoService.class.getName()).log(Level.SEVERE, "Ocurrio un error al eliminar el documento.", ex);
            return new Respuesta(false, "Ocurrio un error al eliminar el documento.", "eliminarDocumento " + ex.getMessage());
        }
    }
}
