/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.service;

import cr.ac.una.sigeceuna.model.ActividadDto;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.Request;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Lesmi
 */
public class ActividadService {

    public Respuesta getActividad(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "ActividadController/actividad", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            ActividadDto actividadDto = (ActividadDto) request.readEntity(ActividadDto.class);
            return new Respuesta(true, "", "", "Actividad", actividadDto);

        } catch (Exception ex) {
            Logger.getLogger(ActividadService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar la actividad.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar la actividad.", "getActividad " + ex.getMessage());
        }
    }

    public Respuesta cambiarPrioridad(Long id, Long prioridad) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            parametros.put("prioridad", prioridad);
            Request request = new Request(AppContext.getInstance().get("resturl") + "ActividadController/actividad", "/{id}/{prioridad}", parametros);
            request.put(id);

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            ActividadDto actividadDto = (ActividadDto) request.readEntity(ActividadDto.class);
            return new Respuesta(true, "", "", "Actividad", actividadDto);

        } catch (Exception ex) {
            Logger.getLogger(ActividadService.class.getName()).log(Level.SEVERE, "Ocurrio un error al cambiar la prioridad.", ex);
            return new Respuesta(false, "Ocurrio un error al cambiar la prioridad.", "cambiarPrioridad " + ex.getMessage());
        }
    }

    public Respuesta getActividades() {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "ActividadController/actividades");
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<ActividadDto> actividades = (List<ActividadDto>) request.readEntity(new GenericType<List<ActividadDto>>() {
            });
            return new Respuesta(true, "", "", "Actividades", actividades);
        } catch (Exception ex) {
            Logger.getLogger(ActividadService.class.getName()).log(Level.SEVERE, "Error obteniendo las actividades.", ex);
            return new Respuesta(false, "Error obteniendo las actividades.", "getActividades " + ex.getMessage());
        }
    }

    public Respuesta guardarActividad(ActividadDto actividad) {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "ActividadController/actividad");
            request.post(actividad);
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            ActividadDto actividadDto = (ActividadDto) request.readEntity(ActividadDto.class);
            return new Respuesta(true, "", "", "Actividad", actividadDto);

        } catch (Exception ex) {
            Logger.getLogger(ActividadService.class.getName()).log(Level.SEVERE, "Ocurrio un error al guardar la actividad.", ex);
            return new Respuesta(false, "Ocurrio un error al guardar la actividad.", "guardarActividad " + ex.getMessage());
        }
    }

    public Respuesta eliminarActividad(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "ActividadController/actividad", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(ActividadService.class.getName()).log(Level.SEVERE, "Ocurrio un error al eliminar la actividad.", ex);
            return new Respuesta(false, "Ocurrio un error al eliminar la actividad.", "eliminarActividad " + ex.getMessage());
        }
    }
}
