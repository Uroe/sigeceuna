/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.service;

import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.Request;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lesmi
 */
public class ReporteService {

    public Respuesta getReporte(Long id, String fecha1, String fecha2) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            parametros.put("fecha1", fecha1);
            parametros.put("fecha2", fecha2);
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/reporte", "/{id}/{fecha1}/{fecha2}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            String xml = (String) request.readEntity(String.class);

            return new Respuesta(true, "", "", "Reporte", xml);

        } catch (Exception ex) {
            Logger.getLogger(ReporteService.class.getName()).log(Level.SEVERE, "Ocurrio un error al obtener el reporte.", ex);
            return new Respuesta(false, "Ocurrio un error al obtener el reporte.", "getReporte " + ex.getMessage());
        }
    }

    public Respuesta getReporteAreas() {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/reporteareas");
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            String xml = (String) request.readEntity(String.class);

            return new Respuesta(true, "", "", "Reporte", xml);

        } catch (Exception ex) {
            Logger.getLogger(ReporteService.class.getName()).log(Level.SEVERE, "Ocurrio un error al obtener el reporte.", ex);
            return new Respuesta(false, "Ocurrio un error al obtener el reporte.", "getReporteAreas " + ex.getMessage());
        }
    }

    public Respuesta getReporteEmpleados(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/reporteempleados", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            String xml = (String) request.readEntity(String.class);

            return new Respuesta(true, "", "", "Reporte", xml);

        } catch (Exception ex) {
            Logger.getLogger(ReporteService.class.getName()).log(Level.SEVERE, "Ocurrio un error al obtener el reporte.", ex);
            return new Respuesta(false, "Ocurrio un error al obtener el reporte.", "getReporteEmpleados " + ex.getMessage());
        }
    }
}
