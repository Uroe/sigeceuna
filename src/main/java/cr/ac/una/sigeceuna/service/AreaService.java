/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.service;

import cr.ac.una.sigeceuna.model.AreaDto;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.Request;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author JosueNG
 */
public class AreaService {
    
    public Respuesta getArea(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "AreaController/area", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            AreaDto areaDto = (AreaDto) request.readEntity(AreaDto.class);
            return new Respuesta(true, "", "", "Area", areaDto);

        } catch (Exception ex) {
            Logger.getLogger(AreaService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar la area.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar la area.", "getArea " + ex.getMessage());
        }
    }

    public Respuesta getAreas() {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "AreaController/areas");
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<AreaDto> areas = (List<AreaDto>) request.readEntity(new GenericType<List<AreaDto>>() {
            });
            return new Respuesta(true, "", "", "Areas", areas);
        } catch (Exception ex) {
            Logger.getLogger(AreaService.class.getName()).log(Level.SEVERE, "Error obteniendo las areas.", ex);
            return new Respuesta(false, "Error obteniendo las areas.", "getAreas " + ex.getMessage());
        }
    }

    public Respuesta guardarArea(AreaDto area) {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "AreaController/area");
            request.post(area);
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            AreaDto areaDto = (AreaDto) request.readEntity(AreaDto.class);
            return new Respuesta(true, "", "", "Area", areaDto);

        } catch (Exception ex) {
            Logger.getLogger(AreaService.class.getName()).log(Level.SEVERE, "Ocurrio un error al guardar la area.", ex);
            return new Respuesta(false, "Ocurrio un error al guardar la area.", "guardarArea " + ex.getMessage());
        }
    }

    public Respuesta eliminarArea(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "AreaController/area", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(AreaService.class.getName()).log(Level.SEVERE, "Ocurrio un error al eliminar la area.", ex);
            return new Respuesta(false, "Ocurrio un error al eliminar la area.", "eliminarArea " + ex.getMessage());
        }
    }
}
