/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.service;

import cr.ac.una.sigeceuna.model.SeguimientoDto;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.Request;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Lesmi
 */
public class SeguimientoService {

    public Respuesta getSeguimiento(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "SeguimientoController/seguimiento", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            SeguimientoDto seguimientoDto = (SeguimientoDto) request.readEntity(SeguimientoDto.class);
            return new Respuesta(true, "", "", "Seguimiento", seguimientoDto);

        } catch (Exception ex) {
            Logger.getLogger(SeguimientoService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar el seguimiento.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar el seguimiento.", "getSeguimiento " + ex.getMessage());
        }
    }

    public Respuesta getSeguimientos(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "SeguimientoController/seguimientos", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<SeguimientoDto> seguimientoDtos = (List<SeguimientoDto>) request.readEntity(new GenericType<List<SeguimientoDto>>() {
            });
            return new Respuesta(true, "", "", "Seguimientos", seguimientoDtos);
        } catch (Exception ex) {
            Logger.getLogger(SeguimientoService.class.getName()).log(Level.SEVERE, "Error obteniendo seguimientos.", ex);
            return new Respuesta(false, "Error obteniendo seguimientos.", "getSeguimientos " + ex.getMessage());
        }
    }

    public Respuesta guardarSeguimiento(SeguimientoDto seguimiento) {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "SeguimientoController/seguimiento");
            request.post(seguimiento);
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            SeguimientoDto seguimientoDto = (SeguimientoDto) request.readEntity(SeguimientoDto.class);
            return new Respuesta(true, "", "", "Seguimiento", seguimientoDto);

        } catch (Exception ex) {
            Logger.getLogger(SeguimientoService.class.getName()).log(Level.SEVERE, "Ocurrio un error al guardar el seguimiento.", ex);
            return new Respuesta(false, "Ocurrio un error al guardar el seguimiento.", "guardarSeguimiento" + ex.getMessage());
        }
    }

    public Respuesta eliminarSeguimiento(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "SeguimientoController/seguimiento", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(SeguimientoService.class.getName()).log(Level.SEVERE, "Ocurrio un error al eliminar el seguimiento.", ex);
            return new Respuesta(false, "Ocurrio un error al eliminar el seguimiento.", "eliminarSeguimiento " + ex.getMessage());
        }
    }
}
