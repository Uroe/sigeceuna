/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.service;

import cr.ac.una.sigeceuna.model.EmpleadoDto;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.Request;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author JosueNG
 */
public class EmpleadoService {

    public Respuesta getEmpleado(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "EmpleadoController/empleado", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            EmpleadoDto empleado = (EmpleadoDto) request.readEntity(EmpleadoDto.class);
            return new Respuesta(true, "", "", "Empleado", empleado);
        } catch (Exception ex) {
            Logger.getLogger(EmpleadoService.class.getName()).log(Level.SEVERE, "Error obteniendo el empleado [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo el empleado.", "getEmpleado " + ex.getMessage());
        }
    }

    public Respuesta getEmpleados() {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "EmpleadoController/empleados");
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<EmpleadoDto> empleadoDtos = (List<EmpleadoDto>) request.readEntity(new GenericType<List<EmpleadoDto>>() {
            });
            return new Respuesta(true, "", "", "Empleados", empleadoDtos);
        } catch (Exception ex) {
            Logger.getLogger(EmpleadoService.class.getName()).log(Level.SEVERE, "Error obteniendo empleados.", ex);
            return new Respuesta(false, "Error obteniendo empleados.", "getEmpleados " + ex.getMessage());
        }
    }

    public Respuesta guardarEmpleado(EmpleadoDto empleado) {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "EmpleadoController/empleado");
            request.post(empleado);

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            EmpleadoDto empleadoDto = (EmpleadoDto) request.readEntity(EmpleadoDto.class);
            return new Respuesta(true, "", "", "Empleado", empleadoDto);
        } catch (Exception ex) {
            Logger.getLogger(EmpleadoService.class.getName()).log(Level.SEVERE, "Error guardando el empleado.", ex);
            return new Respuesta(false, "Error guardando el empleado.", "guardarEmpleado " + ex.getMessage());
        }
    }

    public Respuesta eliminarEmpleado(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "EmpleadoController/empleado", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(EmpleadoService.class.getName()).log(Level.SEVERE, "Error eliminando el empleado.", ex);
            return new Respuesta(false, "Error eliminando el empleado.", "eliminarEmpleado " + ex.getMessage());
        }
    }
}
