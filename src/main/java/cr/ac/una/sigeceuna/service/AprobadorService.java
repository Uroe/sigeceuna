/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.service;

import cr.ac.una.sigeceuna.model.AprobadorDto;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.Request;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Lesmi
 */
public class AprobadorService {

    public Respuesta getAprobador(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "AprobadorController/aprobador", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            AprobadorDto aprobadorDto = (AprobadorDto) request.readEntity(AprobadorDto.class);
            return new Respuesta(true, "", "", "Aprobador", aprobadorDto);

        } catch (Exception ex) {
            Logger.getLogger(AprobadorService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar el aprobador.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar el aprobador.", "getAprobador " + ex.getMessage());
        }
    }

    public Respuesta getAprobadores() {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "AprobadorController/aprobadores");
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<AprobadorDto> aprobadorDtos = (List<AprobadorDto>) request.readEntity(new GenericType<List<AprobadorDto>>() {
            });
            return new Respuesta(true, "", "", "Aprobadores", aprobadorDtos);
        } catch (Exception ex) {
            Logger.getLogger(EmpleadoService.class.getName()).log(Level.SEVERE, "Error obteniendo aprobadores.", ex);
            return new Respuesta(false, "Error obteniendo aprobadores.", "getAprobadores " + ex.getMessage());
        }
    }

    public Respuesta guardarAprobador(AprobadorDto aprobador) {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "AprobadorController/aprobador");
            request.post(aprobador);
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            AprobadorDto aprobadorDto = (AprobadorDto) request.readEntity(AprobadorDto.class);
            return new Respuesta(true, "", "", "Aprobador", aprobadorDto);

        } catch (Exception ex) {
            Logger.getLogger(AprobadorService.class.getName()).log(Level.SEVERE, "Ocurrio un error al guardar el aprobador.", ex);
            return new Respuesta(false, "Ocurrio un error al guardar el aprobador.", "guardarAprobador " + ex.getMessage());
        }
    }

    public Respuesta eliminarAprobador(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "AprobadorController/aprobador", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(AprobadorService.class.getName()).log(Level.SEVERE, "Ocurrio un error al eliminar el aprobador.", ex);
            return new Respuesta(false, "Ocurrio un error al eliminar el aprobador.", "eliminarAprobador " + ex.getMessage());
        }
    }
}
