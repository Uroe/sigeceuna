/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.service;

import cr.ac.una.sigeceuna.model.GestionDto;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.Request;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Lesmi
 */
public class GestionService {

    public Respuesta getGestion(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/gestion", "/{id}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            GestionDto gestionDto = (GestionDto) request.readEntity(GestionDto.class);
            return new Respuesta(true, "", "", "Gestion", gestionDto);

        } catch (Exception ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, "Ocurrio un error al consultar la gestion.", ex);
            return new Respuesta(false, "Ocurrio un error al consultar la gestion.", "getGestion " + ex.getMessage());
        }
    }

    public Respuesta cambiarEstado(Long id, String estado) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            parametros.put("estado", estado);
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/gestion", "/{id}/{estado}", parametros);
            request.put(id);

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            GestionDto gestionDto = (GestionDto) request.readEntity(GestionDto.class);
            return new Respuesta(true, "", "", "Gestion", gestionDto);

        } catch (Exception ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, "Ocurrio un error al cambiar el estado.", ex);
            return new Respuesta(false, "Ocurrio un error al cambiar el estado.", "cambiarEstado " + ex.getMessage());
        }
    }

    public Respuesta asignarFsolucion(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/gestion", "/{id}", parametros);
            request.put(id);

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            GestionDto gestionDto = (GestionDto) request.readEntity(GestionDto.class);
            return new Respuesta(true, "", "", "Gestion", gestionDto);

        } catch (Exception ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, "Ocurrio un error al asignar la fecha.", ex);
            return new Respuesta(false, "Ocurrio un error al asignar la fecha.", "asignarFsolucion " + ex.getMessage());
        }
    }

    public Respuesta getGestiones() {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/gestiones");
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<GestionDto> gestiones = (List<GestionDto>) request.readEntity(new GenericType<List<GestionDto>>() {
            });
            return new Respuesta(true, "", "", "Gestiones", gestiones);
        } catch (Exception ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, "Error obteniendo gestiones.", ex);
            return new Respuesta(false, "Error obteniendo gestiones.", "getGestiones " + ex.getMessage());
        }
    }

    public Respuesta getGestionesMes(int mes) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("mes", mes);
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/gestionesmes", "/{mes}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<GestionDto> gestiones = (List<GestionDto>) request.readEntity(new GenericType<List<GestionDto>>() {
            });
            return new Respuesta(true, "", "", "GestionesMes", gestiones);
        } catch (Exception ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, "Error obteniendo gestiones.", ex);
            return new Respuesta(false, "Error obteniendo gestiones.", "getGestiones " + ex.getMessage());
        }
    }

    public Respuesta getGestionesSolicitadas(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idSoli", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/gestionessoli", "/{idSoli}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<GestionDto> gestiones = (List<GestionDto>) request.readEntity(new GenericType<List<GestionDto>>() {
            });
            return new Respuesta(true, "", "", "Gestiones", gestiones);
        } catch (Exception ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, "Error obteniendo gestiones.", ex);
            return new Respuesta(false, "Error obteniendo gestiones.", "getGestiones " + ex.getMessage());
        }
    }

    public Respuesta getGestionesSoliFin(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idSoli", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/gestionessolifin", "/{idSoli}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<GestionDto> gestiones = (List<GestionDto>) request.readEntity(new GenericType<List<GestionDto>>() {
            });
            return new Respuesta(true, "", "", "Gestiones", gestiones);
        } catch (Exception ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, "Error obteniendo gestiones.", ex);
            return new Respuesta(false, "Error obteniendo gestiones.", "getGestiones " + ex.getMessage());
        }
    }

    public Respuesta getGestionesAsignadas(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("idAsig", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/gestionesasig", "/{idAsig}", parametros);
            request.get();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            List<GestionDto> gestiones = (List<GestionDto>) request.readEntity(new GenericType<List<GestionDto>>() {
            });
            return new Respuesta(true, "", "", "Gestiones", gestiones);
        } catch (Exception ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, "Error obteniendo gestiones.", ex);
            return new Respuesta(false, "Error obteniendo gestiones.", "getGestiones " + ex.getMessage());
        }
    }

    public Respuesta guardarGestion(GestionDto gestion) {
        try {
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/gestion");
            request.post(gestion);
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }

            GestionDto gestionDto = (GestionDto) request.readEntity(GestionDto.class);
            return new Respuesta(true, "", "", "Gestion", gestionDto);

        } catch (Exception ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, "Ocurrio un error al guardar la gestion.", ex);
            return new Respuesta(false, "Ocurrio un error al guardar la gestion.", "guardarGestion " + ex.getMessage());
        }
    }

    public Respuesta eliminarGestion(Long id) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("id", id);
            Request request = new Request(AppContext.getInstance().get("resturl") + "GestionController/gestion", "/{id}", parametros);
            request.delete();

            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, "Ocurrio un error al eliminar la gestion.", ex);
            return new Respuesta(false, "Ocurrio un error al eliminar la gestion.", "eliminarGestion " + ex.getMessage());
        }
    }

    public Respuesta notificar(String para, String mensaje) {
        try {
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("para", para);
            parametros.put("mensaje", mensaje);
            Request request = new Request(AppContext.getInstance().get("correosurl") + "CorreoController/correo", "/{para}/{mensaje}", parametros);
            request.get();
            if (request.isError()) {
                return new Respuesta(false, request.getError(), "");
            }
            return new Respuesta(true, "", "");

        } catch (Exception ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, "Ocurrio un error al enviar el correo.", ex);
            return new Respuesta(false, "Ocurrio un error al enviar el correo.", "notificar " + ex.getMessage());
        }
    }
}
