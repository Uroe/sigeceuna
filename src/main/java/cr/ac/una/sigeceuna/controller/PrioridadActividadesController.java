/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import cr.ac.una.sigeceuna.model.ActividadDto;
import cr.ac.una.sigeceuna.service.ActividadService;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Lesmi
 */
public class PrioridadActividadesController extends Controller implements Initializable {

    @FXML
    private HBox hbPriori1;
    @FXML
    private HBox hbPriori2;
    @FXML
    private HBox hbPriori3;
    @FXML
    private HBox hbPriori4;
    @FXML
    private HBox hbPriori5;
    @FXML
    private HBox hbPriori6;

    HBox h;
    
    ObservableList<HBox> listaAct = FXCollections.observableArrayList();
    
    ObservableList<HBox> listaPrioridad = FXCollections.observableArrayList();
    
    List<Label> labels = new ArrayList<>();
    
    List<ActividadDto> actividades = new ArrayList<>();
    
    ActividadDto act;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        ActividadService service = new ActividadService();
        Respuesta resp = service.getActividades();
        if (resp.getEstado()) {
            actividades = (List<ActividadDto>) resp.getResultado("Actividades");
        }

        listaPrioridad.addAll(hbPriori1, hbPriori2, hbPriori3, hbPriori4, hbPriori5, hbPriori6);

        for (int i = 0; i < actividades.size(); i++) {
            listaAct.add(new HBox());
        }
        for (ActividadDto act : actividades) {
            labels.add(new Label(act.getNombre()));
        }
        for (Label label : labels) {
            label.getStyleClass().add("jfx-subtitulo-label2");
        }
        for (HBox act : listaAct) {
            act.getStyleClass().add("hbPriori-actividad");
            act.getStyleClass().add("drag");
        }
        for (int i = 0; i < actividades.size(); i++) {
            listaAct.get(i).getChildren().add(labels.get(i));
        }

        for (int i = 0; i < actividades.size(); i++) {
            if (actividades.get(i).getPrioridad() == 1) {
                hbPriori1.getChildren().add(listaAct.get(i));
            }
            if (actividades.get(i).getPrioridad() == 2) {
                hbPriori2.getChildren().add(listaAct.get(i));
            }
            if (actividades.get(i).getPrioridad() == 3) {
                hbPriori3.getChildren().add(listaAct.get(i));
            }
            if (actividades.get(i).getPrioridad() == 4) {
                hbPriori4.getChildren().add(listaAct.get(i));
            }
            if (actividades.get(i).getPrioridad() == 5) {
                hbPriori5.getChildren().add(listaAct.get(i));
            }
            if (actividades.get(i).getPrioridad() == 6) {
                hbPriori6.getChildren().add(listaAct.get(i));
            }
        }

        arrastrar();
    }

    @Override
    public void initialize() {

    }

    private void arrastrar() {
        listaAct.forEach((HBox t) -> {
            t.setOnDragDetected((MouseEvent e) -> {
                Dragboard db = t.startDragAndDrop(TransferMode.MOVE);
                ClipboardContent content = new ClipboardContent();
                content.put(DataFormat.IMAGE, t);
                db.setContent(content);
                h = t;
                e.consume();
            });
        });
        listaPrioridad.forEach((t) -> {
            t.setOnDragOver((DragEvent event) -> {
                if (t != h.getParent()) {
                    event.acceptTransferModes(TransferMode.MOVE);
                }
                event.consume();
            });
        });

        listaPrioridad.forEach((HBox t) -> {
            t.setOnDragDropped((DragEvent t1) -> {
                Dragboard db = t1.getDragboard();
                t.getChildren().add(h);
                Label lbl = (Label) h.getChildren().get(0);
                actividades.forEach((actividad) -> {
                    if (actividad.getNombre().equals(lbl.getText())) {
                        act = actividad;
                    }
                });
                ActividadService service = new ActividadService();
                Respuesta respuesta = service.cambiarPrioridad(act.getId(),
                        Long.valueOf(listaPrioridad.indexOf(t) + 1));
                t1.consume();
            });
        });
    }

}
