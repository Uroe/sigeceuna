/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.sigeceuna.model.ActividadDto;
import cr.ac.una.sigeceuna.model.AreaDto;
import cr.ac.una.sigeceuna.model.SubActividadDto;
import cr.ac.una.sigeceuna.service.ActividadService;
import cr.ac.una.sigeceuna.service.SubActividadService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Formato;
import cr.ac.una.sigeceuna.util.Mensaje;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class ActiSubActiController extends Controller implements Initializable {

    @FXML
    private TabPane tbpActSubAct;
    @FXML
    private Tab tbActividades;
    @FXML
    private JFXTextField txtIDAct;
    @FXML
    private JFXTextField txtNombreAct;
    @FXML
    private TableView<ActividadDto> tbvActividad;
    @FXML
    private TableColumn<ActividadDto, String> tbcIDAct;
    @FXML
    private TableColumn<ActividadDto, String> tbcNombreAct;
    @FXML
    private JFXButton btnNuevoAct;
    @FXML
    private JFXButton btnEliminarAct;
    @FXML
    private JFXButton btnGuardarAct;

    @FXML
    private Tab tbSubActividades;
    @FXML
    private TableView<SubActividadDto> tbvSubActividad;
    @FXML
    private TableColumn<SubActividadDto, String> tbcIDSubAct;
    @FXML
    private TableColumn<SubActividadDto, String> tbcNombreSubAct;
    @FXML
    private JFXTextField txtIDSubAct;
    @FXML
    private JFXTextField txtNombreSubAct;
    @FXML
    private JFXButton btnNuevoSubAct;
    @FXML
    private JFXButton btnEliminarSubAct;
    @FXML
    private JFXButton btnGuardarSubAct;
    @FXML
    private Label lblListActi;
    @FXML
    private Label lblListSubact;

    AreaDto area;

    private ActividadDto actividadDto;

    List<Node> requeridosActi = new ArrayList<>();

    private SubActividadDto subActividadDto;

    List<Node> requeridosSubActi = new ArrayList<>();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        area = (AreaDto) AppContext.getInstance().get("Are");
        lblListActi.setText("Lista de actividades de " + area.getNombre());
        txtIDAct.setTextFormatter(Formato.getInstance().integerFormat());
        txtNombreAct.setTextFormatter(Formato.getInstance().letrasFormat(30));
        actividadDto = new ActividadDto();
        nuevoActividad();
        indicarRequeridosActi();
        tablaListaActi();

        tbcIDAct.setCellValueFactory(cd -> cd.getValue().id);
        tbcNombreAct.setCellValueFactory(cd -> cd.getValue().nombre);

        tbvActividad.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends ActividadDto> observable, ActividadDto oldValue, ActividadDto newValue) -> {
            unbindActividad();
            if (newValue != null) {
                actividadDto = newValue;
                ActividadService as = new ActividadService();
                Respuesta res = as.getActividad(actividadDto.getId());
                actividadDto = (ActividadDto) res.getResultado("Actividad");
                bindActividad(false);
                tablaListaSubActi();
            }
        });

        txtIDSubAct.setTextFormatter(Formato.getInstance().integerFormat());
        txtNombreSubAct.setTextFormatter(Formato.getInstance().letrasFormat(30));
        subActividadDto = new SubActividadDto();
        nuevoSubActividad();
        indicarRequeridosSubActi();

        tbcIDSubAct.setCellValueFactory(cd -> cd.getValue().id);
        tbcNombreSubAct.setCellValueFactory(cd -> cd.getValue().nombre);

        tbvSubActividad.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends SubActividadDto> observable, SubActividadDto oldValue, SubActividadDto newValue) -> {
            unbindSubActividad();
            if (newValue != null) {
                subActividadDto = newValue;
                bindSubActividad(false);
            }
        });
    }

    public void tablaListaActi() {
        tbvActividad.getItems().clear();
        
        tbvActividad.getItems().addAll(area.getActividades());
        tbvActividad.refresh();
    }

    public void indicarRequeridosActi() {
        requeridosActi.clear();
        requeridosActi.addAll(Arrays.asList(txtNombreAct));
    }

    public String validarRequeridosActi() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridosActi) {
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && !((JFXPasswordField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }
    }

    private void bindActividad(Boolean nuevo) {
        if (!nuevo) {
            txtIDAct.textProperty().bind(actividadDto.id);
        }
        txtNombreAct.textProperty().bindBidirectional(actividadDto.nombre);
    }

    private void unbindActividad() {
        txtIDAct.textProperty().unbind();
        txtNombreAct.textProperty().unbindBidirectional(actividadDto.nombre);
    }

    private void nuevoActividad() {
        unbindActividad();
        tbvActividad.getSelectionModel().select(null);
        actividadDto = new ActividadDto();
        bindActividad(true);
        txtIDAct.clear();
        txtIDAct.requestFocus();
    }

    @FXML
    private void onKeyPressedTxtIDAct(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtIDAct.getText().isEmpty()) {
            cargarActividad(Long.valueOf(txtIDAct.getText()));
        }
    }

    private void cargarActividad(Long id) {
        ActividadService service = new ActividadService();
        Respuesta respuesta = service.getActividad(id);

        if (respuesta.getEstado()) {
            unbindActividad();
            actividadDto = (ActividadDto) respuesta.getResultado("Actividad");
            bindActividad(false);
            validarRequeridosActi();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Actividad", getStage(), respuesta.getMensaje());
        }
    }

    @FXML
    private void onActionBtnNuevoAct(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar Actividad", getStage(), "¿Esta seguro que desea limpiar el registro?")) {
            nuevoActividad();
        }
    }

    @FXML
    private void onActionBtnEliminarAct(ActionEvent event) {
        try {
            if (actividadDto.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Actividad", getStage(), "Debe cargar la actividad que desea eliminar.");
            } else {

                ActividadService service = new ActividadService();
                Respuesta respuesta = service.eliminarActividad(actividadDto.getId());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Actividad", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar Actividad", getStage(), "Actividad eliminada correctamente.");
                    nuevoActividad();
                    tablaListaActi();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ActiSubActiController.class.getName()).log(Level.SEVERE, "Error eliminando la actividad.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Actividad", getStage(), "Ocurrio un error eliminando la actividad.");
        }
    }

    @FXML
    private void onActionBtnGuardarAct(ActionEvent event) {
        try {
            String invalidos = validarRequeridosActi();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Actividad", getStage(), invalidos);
            } else {
                actividadDto.setArea(area);
                int p = (int) Math.floor(Math.random() * 6 + 1);
                actividadDto.setPrioridad(Long.valueOf(String.valueOf(p)));
                ActividadService service = new ActividadService();
                Respuesta respuesta = service.guardarActividad(actividadDto);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Actividad", getStage(), respuesta.getMensaje());
                } else {
                    unbindActividad();
                    actividadDto = (ActividadDto) respuesta.getResultado("Actividad");
                    bindActividad(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar Actividad", getStage(), "Actividad actualizada correctamente.");
                    tablaListaActi();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ActiSubActiController.class.getName()).log(Level.SEVERE, "Error guardando la actividad.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Actividad", getStage(), "Ocurrio un error guardando la actividad.");
        }
    }

    @FXML
    private void onSelectionChangedTbSubAct(Event event) {
        if (tbSubActividades.isSelected()) {
            if (actividadDto.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Actividad", getStage(), "Debe cargar un actividad a la que desea agregar un subActividad.");
                tbpActSubAct.getSelectionModel().select(tbActividades);
            }
        } else if (tbActividades.isSelected()) {
            lblListSubact.setText("Lista de subactividades de " + actividadDto.getNombre());
        }
    }

    public void tablaListaSubActi() {
        tbvSubActividad.getItems().clear();
        tbvSubActividad.getItems().addAll(actividadDto.getSubactividades());
        tbvSubActividad.refresh();
    }

    public void indicarRequeridosSubActi() {
        requeridosSubActi.clear();
        requeridosSubActi.addAll(Arrays.asList(txtNombreSubAct));
    }

    public String validarRequeridosSubActi() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridosSubActi) {
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && !((JFXPasswordField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }
    }

    private void bindSubActividad(Boolean nuevo) {
        if (!nuevo) {
            txtIDSubAct.textProperty().bind(subActividadDto.id);
        }
        txtNombreSubAct.textProperty().bindBidirectional(subActividadDto.nombre);
    }

    private void unbindSubActividad() {
        txtIDSubAct.textProperty().unbind();
        txtNombreSubAct.textProperty().unbindBidirectional(subActividadDto.nombre);
    }

    private void nuevoSubActividad() {
        unbindSubActividad();
        tbvSubActividad.getSelectionModel().select(null);
        subActividadDto = new SubActividadDto();
        bindSubActividad(true);
        txtIDSubAct.clear();
        txtIDSubAct.requestFocus();
    }

    @FXML
    private void onKeyPressedTxtIDSubAct(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtIDSubAct.getText().isEmpty()) {
            cargarSubActividad(Long.valueOf(txtIDSubAct.getText()));
        }
    }

    private void cargarSubActividad(Long id) {
        SubActividadService service = new SubActividadService();
        Respuesta respuesta = service.getSubActividad(id);

        if (respuesta.getEstado()) {
            unbindSubActividad();
            subActividadDto = (SubActividadDto) respuesta.getResultado("SubActividad");
            bindSubActividad(false);
            validarRequeridosSubActi();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar SubActividad", getStage(), respuesta.getMensaje());
        }
    }

    @FXML
    private void onActionBtnNuevoSubAct(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar SubActividad", getStage(), "¿Esta seguro que desea limpiar el registro?")) {
            nuevoSubActividad();
        }
    }

    @FXML
    private void onActionBtnEliminarSubAct(ActionEvent event) {
        try {
            if (subActividadDto.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar SubActividad", getStage(), "Debe cargar la subActividad que desea eliminar.");
            } else {

                SubActividadService service = new SubActividadService();
                Respuesta respuesta = service.eliminarSubActividad(subActividadDto.getId());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar SubActividad", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar SubActividad", getStage(), "SubActividad eliminada correctamente.");
                    nuevoSubActividad();
                    tablaListaSubActi();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ActiSubActiController.class.getName()).log(Level.SEVERE, "Error eliminando la subActividad.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar SubActividad", getStage(), "Ocurrio un error eliminando la subActividad.");
        }
    }

    @FXML
    private void onActionBtnGuardarSubAct(ActionEvent event) {
        try {
            String invalidos = validarRequeridosSubActi();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar SubActividad", getStage(), invalidos);
            } else {
                subActividadDto.setActividad(actividadDto);
                int p = (int) Math.floor(Math.random() * 6 + 1);
                subActividadDto.setPrioridad(Long.valueOf(String.valueOf(p)));
                SubActividadService service = new SubActividadService();
                Respuesta respuesta = service.guardarSubActividad(subActividadDto);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar SubActividad", getStage(), respuesta.getMensaje());
                } else {
                    unbindSubActividad();
                    subActividadDto = (SubActividadDto) respuesta.getResultado("SubActividad");
                    bindSubActividad(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar SubActividad", getStage(), "SubActividad actualizada correctamente.");
                    tablaListaSubActi();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ActiSubActiController.class.getName()).log(Level.SEVERE, "Error guardando la subActividad.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar SubActividad", getStage(), "Ocurrio un error guardando la subActividad.");
        }
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void CambiarPrioriActi(ActionEvent event) {
        FlowController.getInstance().goView("PrioridadActividades");
    }

    @FXML
    private void CambiarPrioriSubact(ActionEvent event) {
        FlowController.getInstance().goView("PrioridadSubactividades");
    }
}
