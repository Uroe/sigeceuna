/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.RolDto;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Mensaje;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Hyperlink;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Lesmi
 */
public class LogInController extends Controller implements Initializable {

    @FXML
    private JFXPasswordField pwfContraseña;
    @FXML
    private Hyperlink hypContraseña;
    @FXML
    private JFXButton btnEntrar;
    @FXML
    private JFXButton btnRegistrarse;
    @FXML
    private JFXTextField txtUsuario;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtUsuario.clear();
        pwfContraseña.clear();
        txtUsuario.requestFocus();
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void Entrar(ActionEvent event) {
        try {
            if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnEntrar.getScene().getWindow(), "Es necesario digitar un usuario para ingresar al sistema.");
            } else if (pwfContraseña.getText() == null || pwfContraseña.getText().isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario", (Stage) btnEntrar.getScene().getWindow(), "Es necesario digitar la clave para ingresar al sistema.");
            } else {
                GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
                GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
                UsuarioDto usuarioDto = gestor.validarUsuario(txtUsuario.getText(), pwfContraseña.getText());
                String token = usuarioDto.getToken();
                usuarioDto = gestor.getUsuario(usuarioDto.getUsuId());
                if (!usuarioDto.getRoles().isEmpty()) {
                    for (RolDto role : usuarioDto.getRoles()) {
                        if (role.getRolNombre().equals("Administrador") || role.getRolNombre().equals("Solicitante") || role.getRolNombre().equals("Gestor")) {
                            RolDto rol = gestor.getRol(role.getRolId());
                            if (rol.getSistema().getSisNombre().equals("SigeceUNA")) {
                                AppContext.getInstance().set("Usuario", usuarioDto);
                                AppContext.getInstance().set("Rol", rol);
                                AppContext.getInstance().set("Token", token);
                                if (getStage().getOwner() == null) {
                                    FlowController.getInstance().goMain();
                                    FlowController.getInstance().goView("Personal");
                                }
                                getStage().close();
                            }
                        }
                    }
                } else {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Ingreso", getStage(), "No tiene permiso para acceder"
                            + " a esta aplicacion");
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(LogInController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Ingreso", getStage(),
                    "Sus credenciales no coinciden");
        }
    }

    @FXML
    private void Registrarse(ActionEvent event) {
    }

}
