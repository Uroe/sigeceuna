/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.AprobadorDto;
import cr.ac.una.sigeceuna.service.AprobadorService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Mensaje;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author Lesmi
 */
public class BuscarAprobadoresController extends Controller implements Initializable {

    @FXML
    private SplitMenuButton smbFiltros;
    @FXML
    private JFXTextField txtFiltro;
    @FXML
    private TableView<Usuario> tbvUsuarios;
    @FXML
    private TableColumn<Usuario, String> tbcId;
    @FXML
    private TableColumn<Usuario, String> tbcNombre;
    @FXML
    private TableColumn<Usuario, String> tbcPapellido;
    @FXML
    private TableColumn<Usuario, String> tbcSapellido;
    @FXML
    private TableColumn<Usuario, String> tbcCedula;
    @FXML
    private TableColumn<Usuario, String> tbcRol;
    @FXML
    private JFXButton btnAgregarApro;

    List<Usuario> usuarios = new ArrayList<>();
    List<UsuarioDto> usuarioDtos = new ArrayList<>();
    Usuario aprobador = new Usuario();
    AprobadorDto apro;
    Boolean sel = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        registrarNuevos(gestor);
        usuarioDtos = gestor.getUsuarios();
        for (UsuarioDto u : usuarioDtos) {
            usuarios.add(new Usuario(u));
        }
        if (!usuarios.isEmpty()) {
            tbcId.setCellValueFactory(cd -> cd.getValue().usuId);
            tbcNombre.setCellValueFactory(cd -> cd.getValue().usuNombre);
            tbcPapellido.setCellValueFactory(cd -> cd.getValue().usuPApellido);
            tbcSapellido.setCellValueFactory(cd -> cd.getValue().usuSApellido);
            tbcCedula.setCellValueFactory(cd -> cd.getValue().usuCedula);
            tbvUsuarios.getItems().addAll(usuarios);
        }

        tbvUsuarios.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Usuario> observable, Usuario oldValue, Usuario newValue) -> {
            if (newValue != null) {
                aprobador = newValue;
                AprobadorService aprobadorService = new AprobadorService();
                Respuesta resp = aprobadorService.getAprobador(aprobador.getUsuId());
                apro = (AprobadorDto) resp.getResultado("Aprobador");
                sel = true;
            }
        });
        agregarFiltros(smbFiltros);
    }

    private void agregarFiltros(SplitMenuButton smb) {
        List<MenuItem> items = new ArrayList<>();
        items.add(new MenuItem("Id"));
        items.add(new MenuItem("Nombre"));
        items.add(new MenuItem("Primer apellido"));
        items.add(new MenuItem("Segundo apellido"));
        items.add(new MenuItem("Cedula"));
        smb.getItems().addAll(items);
        smb.getItems().forEach((t) -> {
            t.setOnAction((h) -> {
                smb.setText(t.getText());
            });
        });
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void AgregarAprobador(ActionEvent event) {
        if (sel) {
            AppContext.getInstance().set("Aprobador", apro);
            getStage().close();
            FlowController.getInstance().initialize();
            FlowController.getInstance().goView("Gestiones");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar la gestion", getStage(),
                    "Debe seleccionar un aprobador");
        }
    }

    @FXML
    private void onKeyTypedTxtFiltro(KeyEvent event) {
        ObservableList<Usuario> usu = FXCollections.observableArrayList(usuarios);
        FilteredList<Usuario> filterData = new FilteredList<>(usu, p -> true);
        txtFiltro.textProperty().addListener((ov, t, t1) -> {
            filterData.setPredicate((filtro) -> {
                if (t1 == null || t1.isEmpty()) {
                    return true;
                }
                String typedText = t1.toLowerCase();
                if (filtro.getUsuId().toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Id")) {
                    return true;
                }
                if (filtro.getUsuNombre().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Nombre")) {
                    return true;
                }
                if (filtro.getUsuPApellido().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Primer apellido")) {
                    return true;
                }
                if (filtro.getUsuSApellido().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Segundo apellido")) {
                    return true;
                }
                if (filtro.getUsuCedula().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Cedula")) {
                    return true;
                }
                return false;
            });
            SortedList<Usuario> sortedList = new SortedList<>(filterData);
            sortedList.comparatorProperty().bind(tbvUsuarios.comparatorProperty());
            tbvUsuarios.setItems(sortedList);
        });
    }

    private void registrarNuevos(GestorSeguridadWs gestor) {
        List<UsuarioDto> usuarios = gestor.getUsuarios();
        List<AprobadorDto> aprobadorDtos = new ArrayList<>();
        AprobadorService aproService = new AprobadorService();
        Respuesta respuesta = aproService.getAprobadores();
        if (respuesta.getEstado()) {
            aprobadorDtos = (List<AprobadorDto>) respuesta.getResultado("Aprobadores");
        }
        if (!aprobadorDtos.isEmpty()) {
            for (int i = 0; i < usuarios.size(); i++) {
                Respuesta re = aproService.getAprobador(usuarios.get(i).getUsuId());
                if (!re.getEstado()) {
                    AprobadorDto apro = new AprobadorDto();
                    apro.setId(usuarios.get(i).getUsuId());
                    respuesta = aproService.guardarAprobador(apro);
                }
            }
        } else {
            for (UsuarioDto usu : usuarios) {
                AprobadorDto apro = new AprobadorDto();
                apro.setId(usu.getUsuId());
                respuesta = aproService.guardarAprobador(apro);
            }
        }
    }

}
