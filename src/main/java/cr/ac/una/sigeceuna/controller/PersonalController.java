/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.ActividadDto;
import cr.ac.una.sigeceuna.model.AprobadorDto;
import cr.ac.una.sigeceuna.model.EmpleadoDto;
import cr.ac.una.sigeceuna.model.GestionDto;
import cr.ac.una.sigeceuna.service.ActividadService;
import cr.ac.una.sigeceuna.service.EmpleadoService;
import cr.ac.una.sigeceuna.service.GestionService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Mensaje;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Lesmi
 */
public class PersonalController extends Controller implements Initializable {

    @FXML
    private BorderPane root;
    @FXML
    private HBox root1;
    @FXML
    private JFXButton btnVisPersonal;
    @FXML
    private JFXButton btnAprobar;
    @FXML
    private JFXButton btnAtender;
    @FXML
    private JFXButton btnCurso;
    @FXML
    private JFXButton btnFinalizadas;
    @FXML
    private AnchorPane acpVistaPersonal;
    @FXML
    private Label lblNumEnCurso;
    @FXML
    private Label lblNumPorAprobar;
    @FXML
    private Label lblNumPorAtender;
    @FXML
    private Label lblNumFinalizadas;
    @FXML
    private Label lblNumGestiones;
    @FXML
    private TableView<GestionDto> tbvPersonalView;
    @FXML
    private TableColumn<GestionDto, String> tbcId;
    @FXML
    private TableColumn<GestionDto, String> tbcASolicitud;
    @FXML
    private TableColumn<GestionDto, String> tbcAPerteneciente;
    @FXML
    private TableColumn<GestionDto, String> tbcTiempo;
    @FXML
    private AnchorPane acpAprobar;
    @FXML
    private TableView<GestionDto> tbvGAprobar;
    @FXML
    private TableColumn<GestionDto, String> tbcIdApro;
    @FXML
    private TableColumn<GestionDto, String> tbcAsuntoApro;
    @FXML
    private TableColumn<GestionDto, String> tbcSolicitanteApro;
    @FXML
    private TableColumn<GestionDto, String> tbcFAperturaApro;
    @FXML
    private TableColumn<GestionDto, String> tbcFVencimientoApro;
    @FXML
    private TableColumn<GestionDto, String> tbcAsignadoApro;
    @FXML
    private AnchorPane acpAtender;
    @FXML
    private TableView<GestionDto> tbvGAtenderAten;
    @FXML
    private TableColumn<GestionDto, String> tbcIdAten;
    @FXML
    private TableColumn<GestionDto, String> tbcAsuntoAten;
    @FXML
    private TableColumn<GestionDto, String> tbcSolicitanteAten;
    @FXML
    private TableColumn<GestionDto, String> tbcFAperturaAten;
    @FXML
    private TableColumn<GestionDto, String> tbcFVencimientoAten;
    @FXML
    private AnchorPane acpEnCurso;
    @FXML
    private TableView<GestionDto> tbvGCurso;
    @FXML
    private TableColumn<GestionDto, String> tbcIdCur;
    @FXML
    private TableColumn<GestionDto, String> tbcAsuntoCur;
    @FXML
    private TableColumn<GestionDto, String> tbcFAperturaCur;
    @FXML
    private TableColumn<GestionDto, String> tbcFVencimientoCur;
    @FXML
    private TableColumn<GestionDto, String> tbcAsignadoCur;
    @FXML
    private AnchorPane acpFinalizadas;
    @FXML
    private TableView<GestionDto> tbvGFinalizadas;
    @FXML
    private TableColumn<GestionDto, String> tbcIdFin;
    @FXML
    private TableColumn<GestionDto, String> tbcAsuntoFin;
    @FXML
    private TableColumn<GestionDto, String> tbcFAperturaFin;
    @FXML
    private TableColumn<GestionDto, String> tbcFVencimientoFin;
    @FXML
    private TableColumn<GestionDto, String> tbcAsignadoFin;
    @FXML
    private JFXButton btnDetalleApro;
    @FXML
    private JFXButton btnDetalleAten;
    @FXML
    private JFXButton btnDetalleCur;
    @FXML
    private JFXButton btnDetalleFin;
    @FXML
    private JFXButton btnAprobaciones;

    List<GestionDto> gestiones = new ArrayList<>();
    List<GestionDto> gestionesSoli = new ArrayList<>();
    List<GestionDto> gestionesCurso = new ArrayList<>();
    List<GestionDto> gestionesAsig = new ArrayList<>();
    List<GestionDto> gestionesxApro = new ArrayList<>();
    List<GestionDto> gestionesFin = new ArrayList<>();
    UsuarioDto usuarioDto;
    GestionDto gestionDto;
    GestionService service;
    Respuesta respuesta;
    boolean sel = false;
    GestorSeguridadWs_Service usuarioService = new GestorSeguridadWs_Service();
    GestorSeguridadWs gestor = usuarioService.getGestorSeguridadWsPort();

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        usuarioDto = (UsuarioDto) AppContext.getInstance().get("Usuario");
        service = new GestionService();
        respuesta = service.getGestiones();
        if (respuesta.getEstado()) {
            gestiones = (List<GestionDto>) respuesta.getResultado("Gestiones");
        }
        respuesta = service.getGestionesAsignadas(usuarioDto.getUsuId());
        if (respuesta.getEstado()) {
            gestionesAsig = (List<GestionDto>) respuesta.getResultado("Gestiones");
        }
        respuesta = service.getGestionesSolicitadas(usuarioDto.getUsuId());
        if (respuesta.getEstado()) {
            gestionesSoli = (List<GestionDto>) respuesta.getResultado("Gestiones");
        }
        respuesta = service.getGestionesSoliFin(usuarioDto.getUsuId());
        if (respuesta.getEstado()) {
            gestionesFin = (List<GestionDto>) respuesta.getResultado("Gestiones");
        }

        for (GestionDto gest : gestiones) {
            GestionDto ges;
            respuesta = service.getGestion(gest.getGesId());
            if (respuesta.getEstado()) {
                ges = (GestionDto) respuesta.getResultado("Gestion");
                for (AprobadorDto aprobadore : ges.getAprobadores()) {
                    if (aprobadore.getId().equals(usuarioDto.getUsuId())) {
                        gestionesxApro.add(ges);
                    }
                }
            }
        }

        gestionesCurso = gestionesSoli.stream().filter(g -> g.getGesEstado().equals("C")).collect(
                Collectors.toList());

        lblNumFinalizadas.setText(String.valueOf(gestionesFin.size()));
        lblNumPorAprobar.setText(String.valueOf(gestionesxApro.size()));
        lblNumEnCurso.setText(String.valueOf(gestionesCurso.size()));
        lblNumPorAtender.setText(String.valueOf(gestionesAsig.size()));
        lblNumGestiones.setText(String.valueOf(gestionesxApro.size() + gestionesCurso.size()
                + gestionesAsig.size() + gestionesFin.size()));

        tbcId.setCellValueFactory(cd -> cd.getValue().gesId);

        for (GestionDto gestionesCurso1 : gestionesCurso) {
            respuesta = service.getGestion(gestionesCurso1.getGesId());
            GestionDto ges = (GestionDto) respuesta.getResultado("Gestion");
            ActividadService as = new ActividadService();
            respuesta = as.getActividad(ges.getActividad().getId());
            ActividadDto acDto = (ActividadDto) respuesta.getResultado("Actividad");
            gestionesCurso1.setNomAreaPer(acDto.getArea().getNombre());
        }

        tbcAPerteneciente.setCellValueFactory(cd -> cd.getValue().nomAreaPer);

        for (GestionDto gestionesCurso1 : gestionesCurso) {
            EmpleadoService empleadoService = new EmpleadoService();
            Respuesta respuesta = empleadoService.getEmpleado(gestionesCurso1.getGesPasignada());
            if (respuesta.getEstado()) {
                EmpleadoDto emp = (EmpleadoDto) respuesta.getResultado("Empleado");
                if (!emp.getAreas().isEmpty()) {
                    gestionesCurso1.setNomAreaSoli(emp.getAreas().get(0).getNombre());
                }
            }

        }

        tbcASolicitud.setCellValueFactory(cd -> cd.getValue().nomAreaSoli);

        for (GestionDto gestionesCurso1 : gestionesCurso) {
            int dr = gestionesCurso1.getGesFvencimiento().getDayOfMonth() - LocalDate.now().getDayOfMonth();
            gestionesCurso1.setDiasRestantes(String.valueOf(dr) + " dias restantes");
        }
        tbcTiempo.setCellValueFactory(cd -> cd.getValue().diasRestantes);
        tbvPersonalView.getItems().addAll(gestionesCurso);
    }

    @FXML
    private void onActionBtnDetalleApro(ActionEvent event) {
        if (sel) {
            FlowController.getInstance().goView("DetalleGestiones");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar gestion", getStage(),
                    "Debe seleccionar una gestion");
        }
    }

    @FXML
    private void onActionBtnDetalleAten(ActionEvent event) {
        if (sel) {
            FlowController.getInstance().goView("DetalleGestiones");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar gestion", getStage(),
                    "Debe seleccionar una gestion");
        }
    }

    @FXML
    private void onActionBtnDetalleCur(ActionEvent event) {
        if (sel) {
            FlowController.getInstance().goView("DetalleGestiones");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar gestion", getStage(),
                    "Debe seleccionar una gestion");
        }
    }

    @FXML
    private void onActionBtnDetalleFin(ActionEvent event) {
        if (sel) {
            FlowController.getInstance().goView("DetalleGestiones");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar gestion", getStage(),
                    "Debe seleccionar una gestion");
        }
    }

    @FXML
    private void onActionBtnAprobar(ActionEvent event) {
        tbvGAprobar.getItems().clear();
        acpFinalizadas.setVisible(false);
        acpAtender.setVisible(false);
        acpEnCurso.setVisible(false);
        acpVistaPersonal.setVisible(false);
        acpAprobar.setVisible(true);
        tbcIdApro.setCellValueFactory(cd -> cd.getValue().gesId);
        tbcAsuntoApro.setCellValueFactory(cd -> cd.getValue().gesAsunto);
        tbcFAperturaApro.setCellValueFactory(cd -> cd.getValue().gesFcreacion.asString());
        tbcFVencimientoApro.setCellValueFactory(cd -> cd.getValue().gesFvencimiento.asString());
        for (GestionDto gestionesCurso1 : gestionesxApro) {
            UsuarioDto usu = gestor.getUsuario(gestionesCurso1.getGesSolicitante());
            gestionesCurso1.setNomSolicitante(usu.getUsuNombre());
        }
        for (GestionDto gestionesCurso1 : gestionesxApro) {
            UsuarioDto usu = gestor.getUsuario(gestionesCurso1.getGesPasignada());
            gestionesCurso1.setNomPasignada(usu.getUsuNombre());
        }
        tbcSolicitanteApro.setCellValueFactory(cd -> cd.getValue().nomSolicitante);
        tbcAsignadoApro.setCellValueFactory(cd -> cd.getValue().nomPasignada);

        tbvGAprobar.getItems().addAll(gestionesxApro);
        tbvGAprobar.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends GestionDto> observable, GestionDto oldValue, GestionDto newValue) -> {
            if (newValue != null) {
                gestionDto = newValue;
                respuesta = service.getGestion(gestionDto.getGesId());
                gestionDto = (GestionDto) respuesta.getResultado("Gestion");
                AppContext.getInstance().set("Gestion", gestionDto);
                sel = true;
            }
        });
    }

    @FXML
    private void onActionBtnAtender(ActionEvent event) {
        tbvGAtenderAten.getItems().clear();
        acpAprobar.setVisible(false);
        acpFinalizadas.setVisible(false);
        acpEnCurso.setVisible(false);
        acpVistaPersonal.setVisible(false);
        acpAtender.setVisible(true);
        tbcIdAten.setCellValueFactory(cd -> cd.getValue().gesId);
        tbcAsuntoAten.setCellValueFactory(cd -> cd.getValue().gesAsunto);
        tbcFAperturaAten.setCellValueFactory(cd -> cd.getValue().gesFcreacion.asString());
        tbcFVencimientoAten.setCellValueFactory(cd -> cd.getValue().gesFvencimiento.asString());
        for (GestionDto gestionesCurso1 : gestionesAsig) {
            UsuarioDto usu = gestor.getUsuario(gestionesCurso1.getGesSolicitante());
            gestionesCurso1.setNomSolicitante(usu.getUsuNombre());
        }
        tbcSolicitanteAten.setCellValueFactory(cd -> cd.getValue().nomSolicitante);
        tbvGAtenderAten.getItems().addAll(gestionesAsig);

        tbvGAtenderAten.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends GestionDto> observable, GestionDto oldValue, GestionDto newValue) -> {
            if (newValue != null) {
                gestionDto = newValue;
                respuesta = service.getGestion(gestionDto.getGesId());
                gestionDto = (GestionDto) respuesta.getResultado("Gestion");
                AppContext.getInstance().set("Gestion", gestionDto);
                sel = true;
            }
        });
    }

    @FXML
    private void onActionBtnCurso(ActionEvent event) {
        tbvGCurso.getItems().clear();
        acpAprobar.setVisible(false);
        acpAtender.setVisible(false);
        acpVistaPersonal.setVisible(false);
        acpFinalizadas.setVisible(false);
        acpEnCurso.setVisible(true);
        tbcIdCur.setCellValueFactory(cd -> cd.getValue().gesId);
        tbcAsuntoCur.setCellValueFactory(cd -> cd.getValue().gesAsunto);
        tbcFAperturaCur.setCellValueFactory(cd -> cd.getValue().gesFcreacion.asString());
        tbcFVencimientoCur.setCellValueFactory(cd -> cd.getValue().gesFvencimiento.asString());
        for (GestionDto gestionesCurso1 : gestionesCurso) {
            UsuarioDto usu = gestor.getUsuario(gestionesCurso1.getGesPasignada());
            gestionesCurso1.setNomPasignada(usu.getUsuNombre());
        }
        tbcAsignadoCur.setCellValueFactory(cd -> cd.getValue().nomPasignada);
        tbvGCurso.getItems().addAll(gestionesCurso);

        tbvGCurso.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends GestionDto> observable, GestionDto oldValue, GestionDto newValue) -> {
            if (newValue != null) {
                gestionDto = newValue;
                respuesta = service.getGestion(gestionDto.getGesId());
                gestionDto = (GestionDto) respuesta.getResultado("Gestion");
                AppContext.getInstance().set("Gestion", gestionDto);
                sel = true;
            }
        });
    }

    @FXML
    private void onActionBtnFinalizadas(ActionEvent event) {
        tbvGFinalizadas.getItems().clear();
        acpAprobar.setVisible(false);
        acpAtender.setVisible(false);
        acpEnCurso.setVisible(false);
        acpVistaPersonal.setVisible(false);
        acpFinalizadas.setVisible(true);
        tbcIdFin.setCellValueFactory(cd -> cd.getValue().gesId);
        tbcAsuntoFin.setCellValueFactory(cd -> cd.getValue().gesAsunto);
        tbcFAperturaFin.setCellValueFactory(cd -> cd.getValue().gesFcreacion.asString());
        tbcFVencimientoFin.setCellValueFactory(cd -> cd.getValue().gesFvencimiento.asString());
        for (GestionDto gestionesCurso1 : gestionesFin) {
            UsuarioDto usu = gestor.getUsuario(gestionesCurso1.getGesPasignada());
            gestionesCurso1.setNomPasignada(usu.getUsuNombre());
        }
        tbcAsignadoFin.setCellValueFactory(cd -> cd.getValue().nomPasignada);
        tbvGFinalizadas.getItems().addAll(gestionesFin);
        tbvGFinalizadas.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends GestionDto> observable, GestionDto oldValue, GestionDto newValue) -> {
            if (newValue != null) {
                gestionDto = newValue;
                respuesta = service.getGestion(gestionDto.getGesId());
                gestionDto = (GestionDto) respuesta.getResultado("Gestion");
                AppContext.getInstance().set("Gestion", gestionDto);
                sel = true;
            }
        });
    }

    @FXML
    private void onActionBtnVisPersonal(ActionEvent event) {
        acpAprobar.setVisible(false);
        acpAtender.setVisible(false);
        acpEnCurso.setVisible(false);
        acpFinalizadas.setVisible(false);
        acpVistaPersonal.setVisible(true);
    }

//    private void actualizarEstados() {
//
//        if (!gestiones.isEmpty()) {
//
//            GestionService service = new GestionService();
//            Respuesta respuesta;
//            for (GestionDto gestione : gestiones) {
//                if (gestione.getGesFvencimiento().isBefore(LocalDate.now()) && (!gestione.getGesEstado().equals("S") || !gestione.getGesEstado().equals("A"))) {
//                    respuesta = service.cambiarEstado(gestione.getGesId(), "R");
//                    if (respuesta.getEstado()) {
//                        gestione = (GestionDto) respuesta.getResultado("Gestion");
//                    }
//                }
//            }
//            respuesta = service.getGestiones();
//            gestiones = (List<GestionDto>) respuesta.getResultado("Gestiones");
//        }
//    }
    @Override
    public void initialize() {

    }

    @FXML
    private void onMouseClickedLblEnCurso(MouseEvent event) {
    }

    @FXML
    private void onMouseClickedLblPorAprobar(MouseEvent event) {
    }

    @FXML
    private void onMouseClickedLblPorAtender(MouseEvent event) {
    }

    @FXML
    private void onMouseClickedLblFinalizadas(MouseEvent event) {
    }

    @FXML
    private void onActionBtnAprobaciones(ActionEvent event) {
        FlowController.getInstance().goView("Aprobaciones");
        FlowController.getInstance().initialize();
    }

}
