/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import cr.ac.una.sigeceuna.model.AreaDto;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javax.imageio.ImageIO;
import org.apache.poi.util.IOUtils;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class IconosController extends Controller implements Initializable {

    @FXML
    private ImageView imgvIcono1;
    @FXML
    private ImageView imgvIcono2;
    @FXML
    private ImageView imgvIcono3;
    @FXML
    private ImageView imgvIcono4;
    @FXML
    private ImageView imgvIcono5;
    @FXML
    private ImageView imgvIcono6;
    @FXML
    private ImageView imgvIcono7;
    @FXML
    private ImageView imgvIcono8;
    @FXML
    private ImageView imgvIcono9;

    AreaDto areaDto;
    Image img;
    File archivo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        img = null;
        areaDto = new AreaDto();

    }

    @FXML
    private void onMouseClikedIcono1(MouseEvent event) throws FileNotFoundException, IOException {
        img = imgvIcono1.getImage();
        archivo = new File("../sigeceuna/src/main/resources/cr/ac/una/sigeceuna/resources/Icono1.png");
        BufferedImage imagenFile = ImageIO.read(archivo.getAbsoluteFile());
        ByteArrayOutputStream imagenDb = new ByteArrayOutputStream();
        ImageIO.write(imagenFile, "png", imagenDb);
        byte[] imagen = imagenDb.toByteArray();
        AppContext.getInstance().set("Icono", imagen);
        AppContext.getInstance().set("Imagen", img);
        getStage().close();
        FlowController.getInstance().goView("Areas");
        FlowController.getInstance().initialize();
        
    }

    @FXML
    private void onMouseClikedIcono2(MouseEvent event) throws FileNotFoundException, IOException {
        img = imgvIcono2.getImage();
        archivo = new File("../sigeceuna/src/main/resources/cr/ac/una/sigeceuna/resources/Icono2.png");
        BufferedImage imagenFile = ImageIO.read(archivo.getAbsoluteFile());
        ByteArrayOutputStream imagenDb = new ByteArrayOutputStream();
        ImageIO.write(imagenFile, "png", imagenDb);
        byte[] imagen = imagenDb.toByteArray();
        AppContext.getInstance().set("Icono", imagen);
        AppContext.getInstance().set("Imagen", img);
        getStage().close();
        FlowController.getInstance().goView("Areas");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onMouseClikedIcono3(MouseEvent event) throws FileNotFoundException, IOException{
        img = imgvIcono3.getImage();
        archivo = new File("../sigeceuna/src/main/resources/cr/ac/una/sigeceuna/resources/Icono3.png");
        BufferedImage imagenFile = ImageIO.read(archivo.getAbsoluteFile());
        ByteArrayOutputStream imagenDb = new ByteArrayOutputStream();
        ImageIO.write(imagenFile, "png", imagenDb);
        byte[] imagen = imagenDb.toByteArray();
        AppContext.getInstance().set("Icono", imagen);
        AppContext.getInstance().set("Imagen", img);
        getStage().close();
        FlowController.getInstance().goView("Areas");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onMouseClikedIcono4(MouseEvent event) throws FileNotFoundException, IOException {
        img = imgvIcono4.getImage();
        archivo = new File("../sigeceuna/src/main/resources/cr/ac/una/sigeceuna/resources/Icono4.png");
        BufferedImage imagenFile = ImageIO.read(archivo.getAbsoluteFile());
        ByteArrayOutputStream imagenDb = new ByteArrayOutputStream();
        ImageIO.write(imagenFile, "png", imagenDb);
        byte[] imagen = imagenDb.toByteArray();
        AppContext.getInstance().set("Icono", imagen);
        AppContext.getInstance().set("Imagen", img);
        getStage().close();
        FlowController.getInstance().goView("Areas");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onMouseClikedIcono5(MouseEvent event) throws FileNotFoundException, IOException {
        img = imgvIcono5.getImage();
        archivo = new File("../sigeceuna/src/main/resources/cr/ac/una/sigeceuna/resources/Icono5.png");
        BufferedImage imagenFile = ImageIO.read(archivo.getAbsoluteFile());
        ByteArrayOutputStream imagenDb = new ByteArrayOutputStream();
        ImageIO.write(imagenFile, "png", imagenDb);
        byte[] imagen = imagenDb.toByteArray();
        AppContext.getInstance().set("Icono", imagen);
        AppContext.getInstance().set("Imagen", img);
        getStage().close();
        FlowController.getInstance().goView("Areas");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onMouseClikedIcono6(MouseEvent event) throws FileNotFoundException, IOException {
        img = imgvIcono6.getImage();
        archivo = new File("../sigeceuna/src/main/resources/cr/ac/una/sigeceuna/resources/Icono6.png");
        BufferedImage imagenFile = ImageIO.read(archivo.getAbsoluteFile());
        ByteArrayOutputStream imagenDb = new ByteArrayOutputStream();
        ImageIO.write(imagenFile, "png", imagenDb);
        byte[] imagen = imagenDb.toByteArray();
        AppContext.getInstance().set("Icono", imagen);
        AppContext.getInstance().set("Imagen", img);
        getStage().close();
        FlowController.getInstance().goView("Areas");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onMouseClikedIcono7(MouseEvent event) throws FileNotFoundException, IOException {
        img = imgvIcono7.getImage();
        archivo = new File("../sigeceuna/src/main/resources/cr/ac/una/sigeceuna/resources/Icono7.png");
        BufferedImage imagenFile = ImageIO.read(archivo.getAbsoluteFile());
        ByteArrayOutputStream imagenDb = new ByteArrayOutputStream();
        ImageIO.write(imagenFile, "png", imagenDb);
        byte[] imagen = imagenDb.toByteArray();
        AppContext.getInstance().set("Icono", imagen);
        AppContext.getInstance().set("Imagen", img);
        getStage().close();
        FlowController.getInstance().goView("Areas");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onMouseClikedIcono8(MouseEvent event) throws FileNotFoundException, IOException {
        img = imgvIcono8.getImage();
        archivo = new File("../sigeceuna/src/main/resources/cr/ac/una/sigeceuna/resources/Icono8.png");
        BufferedImage imagenFile = ImageIO.read(archivo.getAbsoluteFile());
        ByteArrayOutputStream imagenDb = new ByteArrayOutputStream();
        ImageIO.write(imagenFile, "png", imagenDb);
        byte[] imagen = imagenDb.toByteArray();
        AppContext.getInstance().set("Icono", imagen);
        AppContext.getInstance().set("Imagen", img);
        getStage().close();
        FlowController.getInstance().goView("Areas");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onMouseClikedIcono9(MouseEvent event) throws FileNotFoundException, IOException {
        img = imgvIcono9.getImage();
        archivo = new File("../sigeceuna/src/main/resources/cr/ac/una/sigeceuna/resources/Icono9.png");
        BufferedImage imagenFile = ImageIO.read(archivo.getAbsoluteFile());
        ByteArrayOutputStream imagenDb = new ByteArrayOutputStream();
        ImageIO.write(imagenFile, "png", imagenDb);
        byte[] imagen = imagenDb.toByteArray();
        AppContext.getInstance().set("Icono", imagen);
        AppContext.getInstance().set("Imagen", img);
        getStage().close();
        FlowController.getInstance().goView("Areas");
        FlowController.getInstance().initialize();
    }

    @Override
    public void initialize() {

    }

}
