package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.ActividadDto;
import cr.ac.una.sigeceuna.model.GestionDto;
import cr.ac.una.sigeceuna.service.ActividadService;
import cr.ac.una.sigeceuna.service.GestionService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.GenerarExcel;
import cr.ac.una.sigeceuna.util.Mensaje;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Lesmi
 */
public class BuscarGestionesController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtFilter;
    @FXML
    private SplitMenuButton smbFiltros;
    @FXML
    private SplitMenuButton smbFiltros1;
    @FXML
    private JFXTextField txtFilter1;
    @FXML
    private SplitMenuButton smbFiltros2;
    @FXML
    private JFXTextField txtFilter2;
    @FXML
    private JFXButton btnResetear;
    @FXML
    private JFXButton btnGestiones;
    @FXML
    private TableView<GestionDto> tbvGestiones;
    @FXML
    private TableColumn<GestionDto, String> tbcId;
    @FXML
    private TableColumn<GestionDto, String> tbcAsunto;
    @FXML
    private TableColumn<GestionDto, String> tbcEstado;
    @FXML
    private TableColumn<GestionDto, LocalDate> tbcFcreacion;
    @FXML
    private TableColumn<GestionDto, LocalDate> tbcFvencimiento;
    @FXML
    private TableColumn<GestionDto, String> tbcSolicitante;
    @FXML
    private TableColumn<GestionDto, String> tbcAsignado;
    @FXML
    private TableColumn<GestionDto, String> tbcArea;
    @FXML
    private TableColumn<GestionDto, String> tbcActividad;
    @FXML
    private TableColumn<GestionDto, String> tbcsubActividad;
    @FXML
    private JFXButton btnEditar;
    @FXML
    private JFXButton btnExcel;

    List<GestionDto> gestionesDto = new ArrayList<>();
    GestionDto gestion = new GestionDto();
    Boolean sel = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        GestionService service = new GestionService();
        Respuesta respuesta = service.getGestiones();
        if (respuesta.getEstado()) {
            gestionesDto = (List<GestionDto>) respuesta.getResultado("Gestiones");
            tbvGestiones.getItems().clear();
            tbcId.setCellValueFactory(cd -> cd.getValue().gesId);
            tbcEstado.setCellValueFactory(cd -> cd.getValue().gesEstado);
            tbcFcreacion.setCellValueFactory(cd -> cd.getValue().gesFcreacion);
            tbcFvencimiento.setCellValueFactory(cd -> cd.getValue().gesFvencimiento);
            tbcAsunto.setCellValueFactory(cd -> cd.getValue().gesAsunto);
            tbcActividad.setCellValueFactory(cd -> cd.getValue().nomActividad);
            tbcArea.setCellValueFactory(cd -> cd.getValue().nomArea);
            tbcsubActividad.setCellValueFactory(cd -> cd.getValue().nomSubactividad);
            tbcSolicitante.setCellValueFactory(cd -> cd.getValue().nomSolicitante);
            tbcAsignado.setCellValueFactory(cd -> cd.getValue().nomPasignada);
            cargarUsuarios();
            cargarAreaActi();
            tbvGestiones.getItems().addAll(gestionesDto);
            agregarFiltros(smbFiltros);
            agregarFiltros(smbFiltros1);
            agregarFiltros(smbFiltros2);
        }
        tbvGestiones.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends GestionDto> observable, GestionDto oldValue, GestionDto newValue) -> {
            if (newValue != null) {
                gestion = newValue;
                Respuesta r = service.getGestion(gestion.getGesId());
                gestion = (GestionDto) r.getResultado("Gestion");
                sel = true;
            }
        });
    }

    private void agregarFiltros(SplitMenuButton smb) {
        List<MenuItem> items = new ArrayList<>();
        items.add(new MenuItem("Id"));
        items.add(new MenuItem("Asunto"));
        items.add(new MenuItem("Estado"));
        items.add(new MenuItem("Fecha de creacion"));
        items.add(new MenuItem("Fecha de vencimiento"));
        items.add(new MenuItem("Solicitante"));
        items.add(new MenuItem("Asignado a"));
        items.add(new MenuItem("Area"));
        items.add(new MenuItem("Actividad"));
        items.add(new MenuItem("Subactividad"));
        smb.getItems().addAll(items);
        smb.getItems().forEach((t) -> {
            t.setOnAction((h) -> {
                smb.setText(t.getText());
            });
        });
    }

    @Override
    public void initialize() {

    }

    private void cargarUsuarios() {
        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        List<UsuarioDto> usuarioDtos = gestor.getUsuarios();
        List<Usuario> usuarios = new ArrayList<>();
        for (UsuarioDto usuarioDto : usuarioDtos) {
            usuarios.add(new Usuario(usuarioDto));
        }
        for (GestionDto gestionesDto1 : gestionesDto) {
            for (Usuario u : usuarios) {
                if (u.getUsuId().equals(gestionesDto1.getGesSolicitante())) {
                    gestionesDto1.setNomSolicitante(u.getUsuNombre());
                }
                if (u.getUsuId().equals(gestionesDto1.getGesPasignada())) {
                    gestionesDto1.setNomPasignada(u.getUsuNombre());
                }
            }
        }
    }

    private void cargarAreaActi() {

        for (GestionDto gestionesDto1 : gestionesDto) {
            GestionService gestionService = new GestionService();
            Respuesta res = gestionService.getGestion(gestionesDto1.getGesId());
            if (res.getEstado()) {
                GestionDto ges = (GestionDto) res.getResultado("Gestion");
                gestionesDto1.setNomActividad(ges.getActividad().getNombre());
                ActividadService as = new ActividadService();
                res = as.getActividad(ges.getActividad().getId());
                if (res.getEstado()) {
                    ActividadDto ad = (ActividadDto) res.getResultado("Actividad");
                    gestionesDto1.setNomArea(ad.getArea().getNombre());
                    gestionesDto1.setNomSubactividad(ges.getSubActividad().getNombre());
                }
            }
        }
    }

    @FXML
    private void onKeyTypedTxtFilter(KeyEvent event) {
        ObservableList<GestionDto> gestiones = FXCollections.observableArrayList(gestionesDto);
        FilteredList<GestionDto> filterData = new FilteredList<>(gestiones, p -> true);
        txtFilter.textProperty().addListener((ov, t, t1) -> {
            filterData.setPredicate((gest) -> {
                if (t1 == null || t1.isEmpty()) {
                    return true;
                }
                String typedText = t1.toLowerCase();
                if (gest.gesId.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Id")) {
                    return true;
                }
                if (gest.gesAsunto.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Asunto")) {
                    return true;
                }
                if (gest.gesEstado.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Estado")) {
                    return true;
                }
                if (gest.nomSolicitante.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Solicitante")) {
                    return true;
                }
                if (gest.gesFcreacion.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Fecha de creacion")) {
                    return true;
                }
                if (gest.gesFvencimiento.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Fecha de vencimiento")) {
                    return true;
                }
                if (gest.nomPasignada.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Asignado a")) {
                    return true;
                }
                if (gest.nomArea.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Area")) {
                    return true;
                }
                if (gest.nomActividad.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Actividad")) {
                    return true;
                }
                if (gest.nomSubactividad.toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Subactividad")) {
                    return true;
                }
                return false;
            });
            SortedList<GestionDto> sortedList = new SortedList<>(filterData);
            sortedList.comparatorProperty().bind(tbvGestiones.comparatorProperty());
            tbvGestiones.setItems(sortedList);
        });
    }

    @FXML
    private void onKeyTypedTxtFilter1(KeyEvent event) {
        ObservableList<GestionDto> gestiones = FXCollections.observableArrayList(gestionesDto);
        FilteredList<GestionDto> filterData = new FilteredList<>(gestiones, p -> true);
        txtFilter1.textProperty().addListener((ov, t, t1) -> {
            filterData.setPredicate((gest) -> {
                if (t1 == null || t1.isEmpty()) {
                    return true;
                }
                String typedText = t1.toLowerCase();
                if (gest.gesId.toString().toLowerCase().contains(typedText) && smbFiltros1.getText().equals("Id")) {
                    return true;
                }
                if (gest.gesAsunto.toString().toLowerCase().contains(typedText) && smbFiltros1.getText().equals("Asunto")) {
                    return true;
                }
                if (gest.gesEstado.toString().toLowerCase().contains(typedText) && smbFiltros1.getText().equals("Estado")) {
                    return true;
                }
                if (gest.nomSolicitante.toString().toLowerCase().contains(typedText) && smbFiltros1.getText().equals("Solicitante")) {
                    return true;
                }
                if (gest.gesFcreacion.toString().toLowerCase().contains(typedText) && smbFiltros1.getText().equals("Fecha de creacion")) {
                    return true;
                }
                if (gest.gesFvencimiento.toString().toLowerCase().contains(typedText) && smbFiltros1.getText().equals("Fecha de vencimiento")) {
                    return true;
                }
                if (gest.nomPasignada.toString().toLowerCase().contains(typedText) && smbFiltros1.getText().equals("Asignado a")) {
                    return true;
                }
                if (gest.nomArea.toString().toLowerCase().contains(typedText) && smbFiltros1.getText().equals("Area")) {
                    return true;
                }
                if (gest.nomActividad.toString().toLowerCase().contains(typedText) && smbFiltros1.getText().equals("Actividad")) {
                    return true;
                }
                if (gest.nomSubactividad.toString().toLowerCase().contains(typedText) && smbFiltros1.getText().equals("Subactividad")) {
                    return true;
                }
                return false;
            });
            SortedList<GestionDto> sortedList = new SortedList<>(filterData);
            sortedList.comparatorProperty().bind(tbvGestiones.comparatorProperty());
            tbvGestiones.setItems(sortedList);
        });
    }

    @FXML
    private void onKeyTypedTxtFilter2(KeyEvent event) {
        ObservableList<GestionDto> gestiones = FXCollections.observableArrayList(gestionesDto);
        FilteredList<GestionDto> filterData = new FilteredList<>(gestiones, p -> true);
        txtFilter2.textProperty().addListener((ov, t, t1) -> {
            filterData.setPredicate((gest) -> {
                if (t1 == null || t1.isEmpty()) {
                    return true;
                }
                String typedText = t1.toLowerCase();
                if (gest.gesId.toString().toLowerCase().contains(typedText) && smbFiltros2.getText().equals("Id")) {
                    return true;
                }
                if (gest.gesAsunto.toString().toLowerCase().contains(typedText) && smbFiltros2.getText().equals("Asunto")) {
                    return true;
                }
                if (gest.gesEstado.toString().toLowerCase().contains(typedText) && smbFiltros2.getText().equals("Estado")) {
                    return true;
                }
                if (gest.nomSolicitante.toString().toLowerCase().contains(typedText) && smbFiltros2.getText().equals("Solicitante")) {
                    return true;
                }
                if (gest.gesFcreacion.toString().toLowerCase().contains(typedText) && smbFiltros2.getText().equals("Fecha de creacion")) {
                    return true;
                }
                if (gest.gesFvencimiento.toString().toLowerCase().contains(typedText) && smbFiltros2.getText().equals("Fecha de vencimiento")) {
                    return true;
                }
                if (gest.nomPasignada.toString().toLowerCase().contains(typedText) && smbFiltros2.getText().equals("Asignado a")) {
                    return true;
                }
                if (gest.nomArea.toString().toLowerCase().contains(typedText) && smbFiltros2.getText().equals("Area")) {
                    return true;
                }
                if (gest.nomActividad.toString().toLowerCase().contains(typedText) && smbFiltros2.getText().equals("Actividad")) {
                    return true;
                }
                if (gest.nomSubactividad.toString().toLowerCase().contains(typedText) && smbFiltros2.getText().equals("Subactividad")) {
                    return true;
                }
                return false;
            });
            SortedList<GestionDto> sortedList = new SortedList<>(filterData);
            sortedList.comparatorProperty().bind(tbvGestiones.comparatorProperty());
            tbvGestiones.setItems(sortedList);
        });
    }

    @FXML
    private void ResetearResultados(ActionEvent event) {
        FlowController.getInstance().goView("BuscarGestiones");
        FlowController.getInstance().initialize();
        txtFilter.requestFocus();
    }

    @FXML
    private void EditarGestion(ActionEvent event) {
        if (sel) {
            AppContext.getInstance().set("Gestion", gestion);
            FlowController.getInstance().initialize();
            FlowController.getInstance().goView("Gestiones");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar la gestion", getStage(),
                    "Debe seleccionar una gestion");
        }
    }

    @FXML
    private void CargarGestion(ActionEvent event) {
        if (sel) {
            AppContext.getInstance().set("Gestion", gestion);
            FlowController.getInstance().initialize();
            FlowController.getInstance().goView("DetalleGestiones");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar la gestion", getStage(),
                    "Debe seleccionar una gestion");
        }
    }

    @FXML
    private void ExportarDatosExcel(ActionEvent event) {
        if (sel) {
            GenerarExcel excel = new GenerarExcel(gestion);
            excel.crearExcel();
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar la gestion", getStage(),
                    "Debe seleccionar una gestion");
        }
    }
}
