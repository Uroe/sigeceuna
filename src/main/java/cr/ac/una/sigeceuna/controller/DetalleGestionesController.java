/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.AprobadorDto;
import cr.ac.una.sigeceuna.model.GestionDto;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.BindingUtils;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Formato;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Ale
 */
public class DetalleGestionesController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    @FXML
    private JFXTextField txtId;
    @FXML
    private JFXTextField txtSolicitante;
    @FXML
    private JFXTextField txtAsunto;
    @FXML
    private JFXTextField txtDescripcion;
    @FXML
    private JFXTextField txtActividad;
    @FXML
    private JFXTextField txtSubactividad;
    @FXML
    private JFXTextField txtAsignado;
    @FXML
    private JFXTextField txtFCreacion;
    @FXML
    private JFXTextField txtFVencimiento;
    @FXML
    private JFXRadioButton rdbAprobacion;
    @FXML
    private ToggleGroup tggEstados;
    @FXML
    private JFXRadioButton rdbRechazada;
    @FXML
    private JFXRadioButton rdbCurso;
    @FXML
    private JFXRadioButton rdbEspera;
    @FXML
    private JFXRadioButton rdbResuelta;
    @FXML
    private TableView<Usuario> tbcAprobadores;
    @FXML
    private TableColumn<Usuario, String> tbcAprobador;
    @FXML
    private JFXRadioButton rdbAnulada;
    @FXML
    private JFXButton btnSeguimientos;

    GestionDto gestion;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtId.setTextFormatter(Formato.getInstance().integerFormat());
        txtAsunto.setTextFormatter(Formato.getInstance().maxLengthFormat(60));
        txtDescripcion.setTextFormatter(Formato.getInstance().maxLengthFormat(200));
        txtSolicitante.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtAsignado.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtFCreacion.setTextFormatter(Formato.getInstance().maxLengthFormat(10));
        txtFVencimiento.setTextFormatter(Formato.getInstance().maxLengthFormat(10));
        txtActividad.setTextFormatter(Formato.getInstance().letrasFormat(40));
        txtSubactividad.setTextFormatter(Formato.getInstance().letrasFormat(50));
        rdbAprobacion.setUserData("P");
        rdbRechazada.setUserData("R");
        rdbCurso.setUserData("C");
        rdbEspera.setUserData("E");
        rdbResuelta.setUserData("S");
        rdbAnulada.setUserData("A");
        gestion = (GestionDto) AppContext.getInstance().get("Gestion");
        cargarSoliAsig();
        cargarAprobadores();
        nuevaGestion();
        tbcAprobador.setCellValueFactory(cd -> cd.getValue().usuNombre);
    }

    private void nuevaGestion() {
        unbindGestion();
        bindGestion();
    }

    private void bindGestion() {
        txtId.textProperty().bind(gestion.gesId);
        txtAsunto.textProperty().bindBidirectional(gestion.gesAsunto);
        txtDescripcion.textProperty().bindBidirectional(gestion.gesDescripcion);
        gestion.setFechaCreacion(gestion.gesFcreacion.getValue().toString());
        txtFCreacion.textProperty().bindBidirectional(gestion.fechaCreacion);
        gestion.setFechaVencimiento(gestion.gesFvencimiento.getValue().toString());
        txtFVencimiento.textProperty().bindBidirectional(gestion.fechaVencimiento);
        txtActividad.textProperty().bindBidirectional(gestion.getActividad().nombre);
        txtSubactividad.textProperty().bindBidirectional(gestion.getSubActividad().nombre);
        txtSolicitante.textProperty().bindBidirectional(gestion.nomSolicitante);
        txtAsignado.textProperty().bindBidirectional(gestion.nomPasignada);
        BindingUtils.bindToggleGroupToProperty(tggEstados, gestion.gesEstado);
    }

    private void unbindGestion() {
        txtId.textProperty().unbind();
        txtAsunto.textProperty().unbindBidirectional(gestion.gesAsunto);
        txtDescripcion.textProperty().unbindBidirectional(gestion.gesDescripcion);
        txtFCreacion.textProperty().unbindBidirectional(gestion.fechaCreacion);
        txtFVencimiento.textProperty().unbindBidirectional(gestion.fechaVencimiento);
        txtActividad.textProperty().unbindBidirectional(gestion.getActividad().nombre);
        txtSubactividad.textProperty().unbindBidirectional(gestion.getSubActividad().nombre);
        txtSolicitante.textProperty().unbindBidirectional(gestion.nomSolicitante);
        txtAsignado.textProperty().unbindBidirectional(gestion.nomPasignada);
        BindingUtils.unbindToggleGroupToProperty(tggEstados, gestion.gesEstado);
    }

    private void cargarSoliAsig() {
        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        UsuarioDto usuario = gestor.getUsuario(gestion.getGesSolicitante());
        gestion.setNomSolicitante(usuario.getUsuNombre() + " " + usuario.getUsuPapellido() + " " + usuario.getUsuSapellido());
        usuario = gestor.getUsuario(gestion.getGesPasignada());
        gestion.setNomPasignada(usuario.getUsuNombre() + " " + usuario.getUsuPapellido() + " " + usuario.getUsuSapellido());
    }

    private void cargarAprobadores() {
        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        List<Usuario> usuarios = new ArrayList<>();

        for (AprobadorDto aprobadore : gestion.getAprobadores()) {
            UsuarioDto usu = gestor.getUsuario(aprobadore.getId());
            usuarios.add(new Usuario(usu));
        }
        tbcAprobadores.getItems().addAll(usuarios);
    }

    @Override
    public void initialize() {
    }

    @FXML
    private void onActionbtnSeguimientos(ActionEvent event) {
        FlowController.getInstance().goView("Seguimientos");
        FlowController.getInstance().initialize();
    }

}
