/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextArea;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.AprobadorDto;
import cr.ac.una.sigeceuna.model.GestionDto;
import cr.ac.una.sigeceuna.model.SeguimientoDto;
import cr.ac.una.sigeceuna.service.AprobadorService;
import cr.ac.una.sigeceuna.service.GestionService;
import cr.ac.una.sigeceuna.service.SeguimientoService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.BindingUtils;
import cr.ac.una.sigeceuna.util.Formato;
import cr.ac.una.sigeceuna.util.Mensaje;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class AprobacionesController extends Controller implements Initializable {

    @FXML
    private TableColumn<GestionDto, String> tbcGestion;
    @FXML
    private TableColumn<GestionDto, String> tbcEstado;
    @FXML
    private Label lblAsuntoGes;
    @FXML
    private JFXRadioButton rdbAprobar;
    @FXML
    private ToggleGroup tggDecision;
    @FXML
    private JFXRadioButton rdbRechazar;
    @FXML
    private JFXTextArea txtAObservaciones;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private TableColumn<Usuario, String> tbcNombre;
    @FXML
    private TableColumn<Usuario, String> tbcCedula;
    @FXML
    private TableColumn<Usuario, String> tbcId;
    @FXML
    private TableView<GestionDto> tbvGestiones;
    @FXML
    private TableView<Usuario> tbvAprobadores;
    @FXML
    private SplitMenuButton smbAprobadores;

    List<GestionDto> gestionesxApro = new ArrayList<>();
    List<UsuarioDto> usuarioDtos = new ArrayList<>();
    List<AprobadorDto> nuevosApro = new ArrayList<>();
    Usuario u;
    GestionDto gestion;
    AprobadorDto aprobador;
    UsuarioDto usuario;
    Boolean sel = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
        txtAObservaciones.setTextFormatter(Formato.getInstance().maxLengthFormat(150));
        rdbAprobar.setUserData("S");
        rdbRechazar.setUserData("N");
        gestion = new GestionDto();
        AprobadorService aproservice = new AprobadorService();
        Respuesta resp = aproservice.getAprobador(usuario.getUsuId());
        aprobador = (AprobadorDto) resp.getResultado("Aprobador");
        validarComentario();
        BindingUtils.bindToggleGroupToProperty(tggDecision, aprobador.aprobacion);

        tbcGestion.setCellValueFactory(cd -> cd.getValue().gesAsunto);
        tbcEstado.setCellValueFactory(cd -> cd.getValue().gesEstado);
        cargarGestiones();

        tbvGestiones.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends GestionDto> observable, GestionDto oldValue, GestionDto newValue) -> {
            if (newValue != null) {
                gestion = newValue;
                GestionService gesService = new GestionService();
                Respuesta respuesta = gesService.getGestion(gestion.getGesId());
                gestion = (GestionDto) respuesta.getResultado("Gestion");
                sel = true;
            }
        });
        tbcId.setCellValueFactory(cd -> cd.getValue().usuId);
        tbcNombre.setCellValueFactory(cd -> cd.getValue().usuNombre);
        tbcCedula.setCellValueFactory(cd -> cd.getValue().usuCedula);
        listaDeAprobadores();
        smbAprobadores.getItems().forEach((t) -> {
            t.setOnAction((e) -> {

                for (UsuarioDto usuarioDto : usuarioDtos) {
                    if ((usuarioDto.getUsuNombre() + " " + usuarioDto.getUsuPapellido() + " "
                            + usuarioDto.getUsuSapellido()).equals(t.getText())) {
                        u = new Usuario(usuarioDto);
                    }
                }
                tbvAprobadores.getItems().add(u);
                AprobadorService as = new AprobadorService();
                Respuesta r = as.getAprobador(u.getUsuId());
                AprobadorDto apro = (AprobadorDto) r.getResultado("Aprobador");
                apro.setModificado(true);
                gestion.getAprobadores().add(apro);
                notificarAprobador();
            });
        });
    }

    private void validarComentario() {
        txtAObservaciones.setDisable(true);
        rdbAprobar.setOnAction((t) -> {
            txtAObservaciones.setDisable(false);
        });
        rdbRechazar.setOnAction((t) -> {
            txtAObservaciones.setDisable(true);
        });
    }

    private void cargarGestiones() {
        gestionesxApro.clear();
        List<GestionDto> gestiones = new ArrayList<>();
        GestionService service = new GestionService();
        Respuesta respuesta = service.getGestiones();
        if (respuesta.getEstado()) {
            gestiones = (List<GestionDto>) respuesta.getResultado("Gestiones");
        }
        for (GestionDto gest : gestiones) {
            GestionDto ges;
            respuesta = service.getGestion(gest.getGesId());
            if (respuesta.getEstado()) {
                ges = (GestionDto) respuesta.getResultado("Gestion");
                for (AprobadorDto aprobadore : ges.getAprobadores()) {
                    if (aprobadore.getId().equals(usuario.getUsuId())) {
                        gestionesxApro.add(ges);
                    }
                }
            }
        }
        tbvGestiones.getItems().clear();
        tbvGestiones.getItems().addAll(gestionesxApro);
        tbvGestiones.refresh();
    }

    private void guardarAprobacion() {
        aprobador.setComentario(txtAObservaciones.getText());
    }

    private void limpiar() {
        txtAObservaciones.clear();
        rdbAprobar.setSelected(false);
        rdbRechazar.setSelected(false);
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        if (sel) {
            try {
                guardarAprobacion();
                validarAprobaciones();
                AprobadorService service = new AprobadorService();
                Respuesta respuesta = service.guardarAprobador(aprobador);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Aprobar gestion", getStage(), respuesta.getMensaje());
                } else {
                    aprobador = (AprobadorDto) respuesta.getResultado("Aprobador");
                    if (rdbAprobar.isSelected()) {
                        new Mensaje().showModal(Alert.AlertType.INFORMATION, "Aprobar gestion", getStage(), "Gestion aprobada correctamente.");
                    } else {
                        new Mensaje().showModal(Alert.AlertType.INFORMATION, "Rechazar gestion", getStage(), "Gestion rechazada correctamente.");
                    }
                    cargarGestiones();
                    limpiar();
                    resetAprobacion();
                }

            } catch (Exception ex) {
                Logger.getLogger(AprobacionesController.class.getName()).log(Level.SEVERE, "Error aprobando la gestion.", ex);
                new Mensaje().showModal(Alert.AlertType.ERROR, "Aprobar gestion", getStage(), "Ocurrio un error aprobando la gestion.");
            }
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar la gestion", getStage(),
                    "Debe seleccionar una gestion");
        }
    }

    private void resetAprobacion() {
        aprobador.setAprobacion("E");
        aprobador.setComentario(null);
        AprobadorService service = new AprobadorService();
        Respuesta respuesta = service.guardarAprobador(aprobador);
    }

    private void validarAprobaciones() {
        gestion.getAprobadoresElim().add(aprobador);
        GestionService gs = new GestionService();
        Respuesta res = gs.guardarGestion(gestion);
        if (res.getEstado()) {
            gestion = (GestionDto) res.getResultado("Gestion");
        }
        GestionService ges = new GestionService();
        if (aprobador.getAprobacion().equals("N")) {
            Respuesta r = ges.cambiarEstado(gestion.getGesId(), "R");
            generarSeguimientoRec();
        } else if (aprobador.getAprobacion().equals("S")) {
            if (!gestion.getGesEstado().equals("S") || !gestion.getGesEstado().equals("A")) {
                Respuesta r = ges.cambiarEstado(gestion.getGesId(), "C");
            }
            generarSeguimientoApro();
        }

    }

    private void generarSeguimientoApro() {
        SeguimientoDto seguimientoDto = new SeguimientoDto();
        seguimientoDto.setGestion(gestion);
        seguimientoDto.setSegFseguimiento(LocalDate.now());
        seguimientoDto.setSegIdusuario(usuario.getUsuId());
        seguimientoDto.setSegDetalle("La gestion ha sido aprobada");
        SeguimientoService seguimientoService = new SeguimientoService();
        Respuesta respuesta = seguimientoService.guardarSeguimiento(seguimientoDto);
    }

    private void generarSeguimientoRec() {
        SeguimientoDto seguimientoDto = new SeguimientoDto();
        seguimientoDto.setGestion(gestion);
        seguimientoDto.setSegFseguimiento(LocalDate.now());
        seguimientoDto.setSegIdusuario(usuario.getUsuId());
        seguimientoDto.setSegDetalle("La gestion ha sido rechazada");
        SeguimientoService seguimientoService = new SeguimientoService();
        Respuesta respuesta = seguimientoService.guardarSeguimiento(seguimientoDto);
    }

    private void listaDeAprobadores() {
        List<MenuItem> items = new ArrayList<>();
        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        registrarNuevos(gestor);
        usuarioDtos = gestor.getUsuarios();
        for (UsuarioDto usuarioDto : usuarioDtos) {
            items.add(new MenuItem(usuarioDto.getUsuNombre() + " " + usuarioDto.getUsuPapellido() + " "
                    + usuarioDto.getUsuSapellido()));
        }
        smbAprobadores.getItems().addAll(items);
    }

    private void registrarNuevos(GestorSeguridadWs gestor) {
        List<UsuarioDto> usuarios = gestor.getUsuarios();
        List<AprobadorDto> aprobadorDtos = new ArrayList<>();
        AprobadorService aproService = new AprobadorService();
        Respuesta respuesta = aproService.getAprobadores();
        if (respuesta.getEstado()) {
            aprobadorDtos = (List<AprobadorDto>) respuesta.getResultado("Aprobadores");
        }
        if (!aprobadorDtos.isEmpty()) {
            for (int i = 0; i < usuarios.size(); i++) {
                Respuesta re = aproService.getAprobador(usuarios.get(i).getUsuId());
                if (!re.getEstado()) {
                    AprobadorDto apro = new AprobadorDto();
                    apro.setId(usuarios.get(i).getUsuId());
                    respuesta = aproService.guardarAprobador(apro);
                }
            }
        } else {
            for (UsuarioDto usu : usuarios) {
                AprobadorDto apro = new AprobadorDto();
                apro.setId(usu.getUsuId());
                respuesta = aproService.guardarAprobador(apro);
            }
        }
    }

    private void notificarAprobador() {
        GestionService service = new GestionService();
        Respuesta res = service.notificar(u.getUsuCorreo(), "Estimado, " + u.getUsuNombre()
                + " Se ha solicitado que apruebe una gestion. ");
    }
}
