/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.GestionDto;
import cr.ac.una.sigeceuna.service.GestionService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class CalendarioM1Controller extends Controller implements Initializable {

    @FXML
    private GridPane gdpOctubre;
    @FXML
    private VBox vb01;
    @FXML
    private VBox vb02;
    @FXML
    private VBox vb03;
    @FXML
    private VBox vb04;
    @FXML
    private VBox vb05;
    @FXML
    private VBox vb06;
    @FXML
    private VBox vb07;
    @FXML
    private VBox vb08;
    @FXML
    private VBox vb09;
    @FXML
    private VBox vb10;
    @FXML
    private VBox vb11;
    @FXML
    private VBox vb12;
    @FXML
    private VBox vb13;
    @FXML
    private VBox vb14;
    @FXML
    private VBox vb15;
    @FXML
    private VBox vb16;
    @FXML
    private VBox vb17;
    @FXML
    private VBox vb18;
    @FXML
    private VBox vb19;
    @FXML
    private VBox vb20;
    @FXML
    private VBox vb21;
    @FXML
    private VBox vb22;
    @FXML
    private VBox vb23;
    @FXML
    private VBox vb24;
    @FXML
    private VBox vb25;
    @FXML
    private VBox vb26;
    @FXML
    private VBox vb27;
    @FXML
    private VBox vb28;
    @FXML
    private VBox vb29;
    @FXML
    private VBox vb30;
    @FXML
    private VBox vb31;
    @FXML
    private JFXButton btnAnterior;
    @FXML
    private JFXButton btnSiguiente;

    UsuarioDto usuario;
    GestionDto gesDto;
    List<GestionDto> gestiones = new ArrayList<>();
    List<VBox> dias = new ArrayList<>();
    GestionService gesService;
    Respuesta r;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
        dias.add(vb01);
        dias.add(vb02);
        dias.add(vb03);
        dias.add(vb04);
        dias.add(vb05);
        dias.add(vb06);
        dias.add(vb07);
        dias.add(vb08);
        dias.add(vb09);
        dias.add(vb10);
        dias.add(vb11);
        dias.add(vb12);
        dias.add(vb13);
        dias.add(vb14);
        dias.add(vb15);
        dias.add(vb16);
        dias.add(vb17);
        dias.add(vb18);
        dias.add(vb19);
        dias.add(vb20);
        dias.add(vb21);
        dias.add(vb22);
        dias.add(vb23);
        dias.add(vb24);
        dias.add(vb25);
        dias.add(vb26);
        dias.add(vb27);
        dias.add(vb28);
        dias.add(vb29);
        dias.add(vb30);
        dias.add(vb31);
        eventoGestion();
    }

    public void eventoGestion() {
        gesService = new GestionService();
        r = gesService.getGestionesAsignadas(usuario.getUsuId());
        if (r.getEstado()) {
            gestiones = (List<GestionDto>) r.getResultado("Gestiones");
        }

        for (int i = 0; i < dias.size(); i++) {
            for (GestionDto gestion : gestiones) {
                if (gestion.getGesFvencimiento().getDayOfMonth() == (i + 1) && gestion.getGesFvencimiento().getMonthValue() == 10) {
                    JFXButton evento = new JFXButton();
                    evento.setText(gestion.getGesAsunto());
                    evento.getStyleClass().add("jfx-btn-evento");
                    Tooltip tl = new Tooltip(gestion.getGesDescripcion());
                    Tooltip.install(evento, tl);

                    evento.setOnAction((t) -> {
                        gesDto = gestion;
                        r = gesService.getGestion(gesDto.getGesId());
                        gesDto = (GestionDto) r.getResultado("Gestion");
                        AppContext.getInstance().set("Gestion", gesDto);
//                        FlowController.getInstance().initialize();
                        FlowController.getInstance().goView("DetalleGestiones");
                        FlowController.getInstance().initialize();
                    });
                    dias.get(i).getChildren().add(evento);
                }
            }
        }
    }

    @Override
    public void initialize() {
    }

    @FXML
    private void onActionBtnAnterior(ActionEvent event) {
    }

    @FXML
    private void onActionBtnSiguiente(ActionEvent event) {
        FlowController.getInstance().goView("CalendarioM2");
    }

}
