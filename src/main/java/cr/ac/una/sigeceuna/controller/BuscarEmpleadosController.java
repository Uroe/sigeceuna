/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.EmpleadoDto;
import cr.ac.una.sigeceuna.service.EmpleadoService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Mensaje;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author Lesmi
 */
public class BuscarEmpleadosController extends Controller implements Initializable {

    @FXML
    private SplitMenuButton smbFiltros;
    @FXML
    private JFXTextField txtFiltro;
    @FXML
    private TableView<Usuario> tbvEmpleados;
    @FXML
    private TableColumn<Usuario, String> tbcId;
    @FXML
    private TableColumn<Usuario, String> tbcNombre;
    @FXML
    private TableColumn<Usuario, String> tbcPapellido;
    @FXML
    private TableColumn<Usuario, String> tbcSapellido;
    @FXML
    private TableColumn<Usuario, String> tbcCedula;
    @FXML
    private TableColumn<Usuario, String> tbcRol;
    @FXML
    private JFXButton btnAgregarEmp;

    List<UsuarioDto> usuarioDtos = new ArrayList<>();
    List<Usuario> usuarios = new ArrayList<>();
    Usuario empleado = new Usuario();
    Boolean sel = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        registrarNuevos(gestor);
        usuarioDtos = gestor.getUsuarios();
        for (UsuarioDto usuarioDto : usuarioDtos) {
            usuarios.add(new Usuario(usuarioDto));
        }

        if (!usuarios.isEmpty()) {
            tbcId.setCellValueFactory(cd -> cd.getValue().usuId);
            tbcNombre.setCellValueFactory(cd -> cd.getValue().usuNombre);
            tbcPapellido.setCellValueFactory(cd -> cd.getValue().usuPApellido);
            tbcSapellido.setCellValueFactory(cd -> cd.getValue().usuSApellido);
            tbcCedula.setCellValueFactory(cd -> cd.getValue().usuCedula);
            tbvEmpleados.getItems().addAll(usuarios);
        }

        tbvEmpleados.getSelectionModel()
                .selectedItemProperty().addListener((ObservableValue<? extends Usuario> observable, Usuario oldValue, Usuario newValue) -> {
                    if (newValue != null) {
                        empleado = newValue;
                        sel = true;
                    }
                });
        agregarFiltros(smbFiltros);
    }

    private void agregarFiltros(SplitMenuButton smb) {
        List<MenuItem> items = new ArrayList<>();
        items.add(new MenuItem("Id"));
        items.add(new MenuItem("Nombre"));
        items.add(new MenuItem("Primer apellido"));
        items.add(new MenuItem("Segundo apellido"));
        items.add(new MenuItem("Cedula"));
        smb.getItems().addAll(items);
        smb.getItems().forEach((t) -> {
            t.setOnAction((h) -> {
                smb.setText(t.getText());
            });
        });
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void AgregarEmpleado(ActionEvent event) {
        if (sel) {
            AppContext.getInstance().set("Empleado", empleado);
            getStage().close();
            FlowController.getInstance().initialize();
            FlowController.getInstance().goView("Areas");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Seleccionar la gestion", getStage(),
                    "Debe seleccionar un empleado");
        }
    }

    @FXML
    private void onKeyTypedTxtFiltro(KeyEvent event) {
        ObservableList<Usuario> usu = FXCollections.observableArrayList(usuarios);
        FilteredList<Usuario> filterData = new FilteredList<>(usu, p -> true);
        txtFiltro.textProperty().addListener((ov, t, t1) -> {
            filterData.setPredicate((filtro) -> {
                if (t1 == null || t1.isEmpty()) {
                    return true;
                }
                String typedText = t1.toLowerCase();
                if (filtro.getUsuId().toString().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Id")) {
                    return true;
                }
                if (filtro.getUsuNombre().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Nombre")) {
                    return true;
                }
                if (filtro.getUsuPApellido().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Primer apellido")) {
                    return true;
                }
                if (filtro.getUsuSApellido().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Segundo apellido")) {
                    return true;
                }
                if (filtro.getUsuCedula().toLowerCase().contains(typedText) && smbFiltros.getText().equals("Cedula")) {
                    return true;
                }
                return false;
            });
            SortedList<Usuario> sortedList = new SortedList<>(filterData);
            sortedList.comparatorProperty().bind(tbvEmpleados.comparatorProperty());
            tbvEmpleados.setItems(sortedList);
        });
    }

    private void registrarNuevos(GestorSeguridadWs gestor) {
        List<UsuarioDto> usuarios = gestor.getUsuarios();
        List<EmpleadoDto> empleadoDtos = new ArrayList<>();
        EmpleadoService empService = new EmpleadoService();
        Respuesta respuesta = empService.getEmpleados();
        if (respuesta.getEstado()) {
            empleadoDtos = (List<EmpleadoDto>) respuesta.getResultado("Empleados");
        }
        if (!empleadoDtos.isEmpty()) {
            for (int i = 0; i < usuarios.size(); i++) {
                Respuesta re = empService.getEmpleado(usuarios.get(i).getUsuId());
                if (!re.getEstado()) {
                    EmpleadoDto emp = new EmpleadoDto();
                    emp.setId(usuarios.get(i).getUsuId());
                    respuesta = empService.guardarEmpleado(emp);
                }
            }
        } else {
            for (UsuarioDto usu : usuarios) {
                EmpleadoDto emp = new EmpleadoDto();
                emp.setId(usu.getUsuId());
                respuesta = empService.guardarEmpleado(emp);
            }
        }
    }

}
