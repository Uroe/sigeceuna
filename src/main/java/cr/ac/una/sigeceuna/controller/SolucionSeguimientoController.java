/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.GestionDto;
import cr.ac.una.sigeceuna.model.SeguimientoDto;
import cr.ac.una.sigeceuna.service.GestionService;
import cr.ac.una.sigeceuna.service.SeguimientoService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Formato;
import cr.ac.una.sigeceuna.util.Mensaje;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author Lesmi
 */
public class SolucionSeguimientoController extends Controller implements Initializable {

    @FXML
    private JFXButton btnResolver;
    @FXML
    private JFXButton btnAnular;
    @FXML
    private TextArea txtDetalle;

    GestionDto gestion;
    SeguimientoDto seguimientoDto;
    UsuarioDto usuario;
    Usuario soli;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
        gestion = (GestionDto) AppContext.getInstance().get("Gestion");
        seguimientoDto = new SeguimientoDto();
        txtDetalle.setTextFormatter(Formato.getInstance().maxLengthFormat(100));
        txtDetalle.textProperty().bindBidirectional(seguimientoDto.segDetalle);
        seguimientoDto.setGestion(gestion);
        seguimientoDto.setSegFseguimiento(LocalDate.now());
        seguimientoDto.setSegIdusuario(usuario.getUsuId());
        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        UsuarioDto usuarioDto = gestor.getUsuario(gestion.getGesSolicitante());
        soli = new Usuario(usuarioDto);
    }

    @FXML
    private void ResolverSolucion(ActionEvent event) {
        if (!txtDetalle.getText().isEmpty()) {
            GestionService gesService = new GestionService();
            Respuesta res = gesService.cambiarEstado(gestion.getGesId(), "S");
            res = gesService.asignarFsolucion(gestion.getGesId());
            SeguimientoService service = new SeguimientoService();
            Respuesta respuesta = service.guardarSeguimiento(seguimientoDto);
            if (respuesta.getEstado()) {
                new Mensaje().showModal(Alert.AlertType.INFORMATION, "Solucion de la gestion",
                        getStage(), "La gestion ha sido resuelta.");
                Respuesta resp = gesService.notificar(soli.getUsuCorreo(), "La gestion " + gestion.getGesAsunto()
                        + " ya ha sido resuelta");
                txtDetalle.clear();

            } else {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Solucion de la gestion",
                        getStage(), "Ocurrio un error resolviendo la gestion.");
            }
        }

    }

    @FXML
    private void AnularSolucion(ActionEvent event) {
        if (!txtDetalle.getText().isEmpty()) {
            GestionService gesService = new GestionService();
            Respuesta res = gesService.cambiarEstado(gestion.getGesId(), "A");
            SeguimientoService service = new SeguimientoService();
            Respuesta respuesta = service.guardarSeguimiento(seguimientoDto);
            if (respuesta.getEstado()) {
                new Mensaje().showModal(Alert.AlertType.INFORMATION, "Solucion de la gestion",
                        getStage(), "La gestion ha sido anulada.");
                Respuesta resp = gesService.notificar(soli.getUsuCorreo(), "La gestion " + gestion.getGesAsunto()
                        + " ha sido anulada");
                txtDetalle.clear();
            } else {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Solucion de la gestion",
                        getStage(), "Ocurrio un error anulando la gestion.");
            }
        }
    }

    @Override
    public void initialize() {

    }

}
