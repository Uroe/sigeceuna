/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.DocumentoDto;
import cr.ac.una.sigeceuna.model.GestionDto;
import cr.ac.una.sigeceuna.model.SeguimientoDto;
import cr.ac.una.sigeceuna.service.DocumentoService;
import cr.ac.una.sigeceuna.service.GestionService;
import cr.ac.una.sigeceuna.service.SeguimientoService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author Lesmi
 */
public class SeguimientosController extends Controller implements Initializable {
    
    @FXML
    private Label lblAsuntoGestion;
    @FXML
    private VBox vbSeguimientos;
    @FXML
    private JFXButton btnCrearSeg;
    @FXML
    private JFXButton btnAdjuntarDoc;
    @FXML
    private JFXButton btnGenSeg;
    
    List<SeguimientoDto> seguimientos = new ArrayList<>();
    List<HBox> hbsgtos = new ArrayList<>();
    List<TextField> detalles = new ArrayList<>();
    List<Label> nombres = new ArrayList<>();
    List<Label> fechas = new ArrayList<>();
    List<VBox> datos = new ArrayList<>();
    SeguimientoDto seguimientoDto;
    GestionDto gestionDto;
    UsuarioDto usuario;
    UsuarioDto solicitante;
    DocumentoDto documentoDto;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
        seguimientoDto = new SeguimientoDto();
        gestionDto = (GestionDto) AppContext.getInstance().get("Gestion");
        GestorSeguridadWs_Service gsservice = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = gsservice.getGestorSeguridadWsPort();
        solicitante = gestor.getUsuario(gestionDto.getGesSolicitante());
        lblAsuntoGestion.setText("Seguimientos de " + gestionDto.getGesAsunto());
        
        SeguimientoService service = new SeguimientoService();
        Respuesta respuesta = service.getSeguimientos(gestionDto.getGesId());
        if (respuesta.getEstado()) {
            seguimientos = (List<SeguimientoDto>) respuesta.getResultado("Seguimientos");
        }
        
        List<SeguimientoDto> segOrdenados = seguimientos.stream().sorted(Comparator.comparing(c -> c.getSegId())).
                collect(Collectors.toList());
        
        for (SeguimientoDto seguimiento : segOrdenados) {
            detalles.add(new TextField(seguimiento.getSegDetalle()));
        }
        for (TextField detalle : detalles) {
            detalle.editableProperty().set(false);
            detalle.getStyleClass().add("txtSeguimiento");
            hbsgtos.add(0, new HBox(detalle));
        }
        for (SeguimientoDto sd : segOrdenados) {
            fechas.add(new Label(sd.getSegFseguimiento().toString()));
        }
        for (SeguimientoDto sd : segOrdenados) {
            UsuarioDto u = gestor.getUsuario(sd.getSegIdusuario());
            nombres.add(new Label(u.getUsuNombre()));
        }

        for (int i = 0; i < fechas.size(); i++) {
            fechas.get(i).getStyleClass().add("jfx-label");
            nombres.get(i).getStyleClass().add("jfx-label");
            datos.add(i, new VBox(fechas.get(i), nombres.get(i)));
        }
        for (HBox seguimiento : hbsgtos) {
            seguimiento.getStyleClass().add("hbSeguimiento");
            seguimiento.spacingProperty().set(10);
        }
        for (int i = 0; i < datos.size(); i++) {
            datos.get(i).setAlignment(Pos.CENTER);
            hbsgtos.get(i).getChildren().add(0, datos.get(i));
        }
        
        vbSeguimientos.getChildren().addAll(hbsgtos);
        
        if (!usuario.getUsuId().equals(gestionDto.getGesPasignada())) {
            btnGenSeg.setDisable(true);
        }
        if(gestionDto.getGesEstado().equals("A") || gestionDto.getGesEstado().equals("S") ){
            btnGenSeg.setDisable(true);
            btnGenSeg.setOpacity(0.4);
        }
    }
    
    @Override
    public void initialize() {
        
    }
    
    @FXML
    private void CrearSeguimiento(ActionEvent event) {
        TextField txtSeg = new TextField();
        txtSeg.getStyleClass().add("txtSeguimiento");
        JFXButton btnEliminar = new JFXButton();
        btnEliminar.getStyleClass().add("jfx-btnimg-tbveliminar");
        JFXButton btnGuardar = new JFXButton();
        btnGuardar.getStyleClass().add("jfx-btnimg-guardar");
        HBox hbox = new HBox();
        hbox.getStyleClass().add("hbSeguimiento");
        hbox.spacingProperty().set(10);
        VBox datos = new VBox();
        Label lblfecha = new Label(LocalDate.now().toString());
        Label lblUsuario = new Label(usuario.getUsuNombre());
        lblfecha.getStyleClass().add("jfx-label");
        lblUsuario.getStyleClass().add("jfx-label");
        datos.getChildren().addAll(lblfecha, lblUsuario);
        datos.setAlignment(Pos.CENTER);
        hbox.getChildren().addAll(datos, txtSeg, btnEliminar, btnGuardar);
        vbSeguimientos.getChildren().add(0, hbox);
        txtSeg.requestFocus();
        btnEliminar.setOnAction((t) -> {
            vbSeguimientos.getChildren().remove(hbox);
        });
        btnGuardar.setOnAction((t) -> {
            seguimientoDto.setSegIdusuario(usuario.getUsuId());
            seguimientoDto.setSegFseguimiento(LocalDate.now());
            seguimientoDto.setSegDetalle(txtSeg.getText());
            seguimientoDto.setGestion(gestionDto);
            SeguimientoService service = new SeguimientoService();
            Respuesta resp = service.guardarSeguimiento(seguimientoDto);
            if (resp.getEstado()) {
                hbox.getChildren().removeAll(btnGuardar);
                txtSeg.editableProperty().set(false);
                notificarSeguimiento();
            }
            
        });
        
    }
    
    private void notificarSeguimiento() {
        GestionService service = new GestionService();
        Respuesta res = service.notificar(solicitante.getUsuCorreo(), "Se ha realizado un seguimiento a la gestion "
                + gestionDto.getGesAsunto());
    }
    
    @FXML
    private void AdjuntarDocumento(ActionEvent event) throws FileNotFoundException, IOException {
        documentoDto = new DocumentoDto();
        FileChooser fileChooser = new FileChooser();
        List<File> selectedFiles = fileChooser.showOpenMultipleDialog(null);
        InputStream in = new FileInputStream(selectedFiles.get(0));
        documentoDto.setDocArchivo(in.readAllBytes());
        documentoDto.setGestion(gestionDto);
        
        TextField txtSeg = new TextField();
        txtSeg.getStyleClass().add("txtSeguimiento");
        JFXButton btnEliminar = new JFXButton();
        btnEliminar.getStyleClass().add("jfx-btnimg-tbveliminar");
        JFXButton btnGuardar = new JFXButton();
        btnGuardar.getStyleClass().add("jfx-btnimg-guardar");
        HBox hbox = new HBox();
        hbox.getStyleClass().add("hbSeguimiento");
        hbox.spacingProperty().set(10);
        hbox.getChildren().addAll(txtSeg, btnEliminar, btnGuardar);
        vbSeguimientos.getChildren().add(0, hbox);
        txtSeg.setText("Documento adjuntado");
        btnEliminar.setOnAction((t) -> {
            vbSeguimientos.getChildren().remove(hbox);
        });
        btnGuardar.setOnAction((t) -> {
            seguimientoDto.setSegIdusuario(usuario.getUsuId());
            seguimientoDto.setSegFseguimiento(LocalDate.now());
            seguimientoDto.setSegDetalle(txtSeg.getText());
            seguimientoDto.setGestion(gestionDto);
            SeguimientoService service = new SeguimientoService();
            Respuesta resp = service.guardarSeguimiento(seguimientoDto);
            if (resp.getEstado()) {
                hbox.getChildren().removeAll(btnGuardar);
                txtSeg.editableProperty().set(false);
                notificarSeguimiento();
                DocumentoService documentoService = new DocumentoService();
                Respuesta respuesta = documentoService.guardarDocumento(documentoDto);
            }
            
        });
    }
    
    @FXML
    private void generarSolucion(ActionEvent event) {
        FlowController.getInstance().goViewInWindow("SolucionSeguimiento");
    }
    
}
