/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.ActividadDto;
import cr.ac.una.sigeceuna.model.AprobadorDto;
import cr.ac.una.sigeceuna.model.AreaDto;
import cr.ac.una.sigeceuna.model.DocumentoDto;
import cr.ac.una.sigeceuna.model.GestionDto;
import cr.ac.una.sigeceuna.model.SeguimientoDto;
import cr.ac.una.sigeceuna.model.SubActividadDto;
import cr.ac.una.sigeceuna.service.ActividadService;
import cr.ac.una.sigeceuna.service.AreaService;
import cr.ac.una.sigeceuna.service.DocumentoService;
import cr.ac.una.sigeceuna.service.GestionService;
import cr.ac.una.sigeceuna.service.SeguimientoService;
import cr.ac.una.sigeceuna.service.SubActividadService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Formato;
import cr.ac.una.sigeceuna.util.Mensaje;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author Lesmi
 */
public class GestionesController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtID;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXTextField txtAsunto;
    @FXML
    private JFXTextField txtSolicitante;
    @FXML
    private JFXTextField txtPasignada;
    @FXML
    private DatePicker dtpFapertura;
    @FXML
    private DatePicker dtpFcierre;
    @FXML
    private TextArea txtDescripcion;
    @FXML
    private SplitMenuButton smbActividad;
    @FXML
    private SplitMenuButton smbSubactividad;
    @FXML
    private JFXButton btnBuscarApro;
    @FXML
    private TableView<Usuario> tbvAprobadores;
    @FXML
    private TableColumn<Usuario, String> tbcNomAprobador;
    @FXML
    private TableColumn<Usuario, Boolean> tbcEliminar;
    @FXML
    private JFXButton btnNuevo;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnBuscarAsig;
    @FXML
    private SplitMenuButton smbAreas;
    @FXML
    private JFXButton btnAdjuntar;
    @FXML
    private TableView<DocumentoDto> tbvDocumentos;
    @FXML
    private TableColumn<DocumentoDto, Boolean> tbcDoc;
    @FXML
    private JFXButton btnBuscar;

    List<AreaDto> areas = new ArrayList<>();
    List<Node> requeridos = new ArrayList<>();
    List<AprobadorDto> aprobadores = new ArrayList<>();
    List<DocumentoDto> documentos = new ArrayList<>();
    List<ActividadDto> actividades = new ArrayList<>();
    List<SubActividadDto> subActividades = new ArrayList<>();
    GestionDto gestion;
    SeguimientoDto seguimiento;
    UsuarioDto usuario;
    UsuarioDto usuarioAsig;
    Usuario solicitante;
    Usuario asignado;
    AreaDto are;
    ActividadDto actividad;
    String cambio;
    DocumentoDto documentoDto;
    UsuarioDto aprobador;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtID.setTextFormatter(Formato.getInstance().integerFormat());
        txtAsunto.setTextFormatter(Formato.getInstance().letrasFormat(80));
        txtDescripcion.setTextFormatter(Formato.getInstance().letrasFormat(200));
        txtPasignada.setTextFormatter(Formato.getInstance().letrasFormat(40));
        txtSolicitante.setTextFormatter(Formato.getInstance().letrasFormat(40));
        gestion = new GestionDto();
        seguimiento = new SeguimientoDto();
        nuevaGestion();
        indicarRequeridos();
        cargarAreas();
        guardarAprobadores();
        restaurarInfo();

        if (AppContext.getInstance().get("Usuario") != null) {
            usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
            solicitante = new Usuario(usuario);
            txtSolicitante.setText(usuario.getUsuNombre() + " " + usuario.getUsuPapellido() + " " + usuario.getUsuSapellido());
            txtSolicitante.editableProperty().set(false);
        }

        if (AppContext.getInstance().get("Gestion") != null) {
            gestion = (GestionDto) AppContext.getInstance().get("Gestion");
            cargarGestion(gestion.getGesId());
        }

        if (AppContext.getInstance().get("Asignado") != null) {
            asignado = (Usuario) AppContext.getInstance().get("Asignado");
            txtPasignada.setText(asignado.getUsuNombre() + " " + asignado.getUsuPApellido() + " " + asignado.getUsuSApellido());
        }

        tbcNomAprobador.setCellValueFactory(cd -> cd.getValue().usuNombre);
        tbcEliminar.setCellValueFactory((TableColumn.CellDataFeatures< Usuario, Boolean> p)
                -> new SimpleBooleanProperty(p.getValue() != null));

        tbcEliminar.setCellFactory((TableColumn<Usuario, Boolean> p) -> new GestionesController.ButtonCell());

        tbcDoc.setCellValueFactory((TableColumn.CellDataFeatures< DocumentoDto, Boolean> p)
                -> new SimpleBooleanProperty(p.getValue() != null));

        tbcDoc.setCellFactory((TableColumn<DocumentoDto, Boolean> p) -> new GestionesController.BtnCell());

        if (!aprobadores.isEmpty()) {
            List<Usuario> listaAprobadores = new ArrayList<>();
            tbvAprobadores.getItems().clear();
            for (AprobadorDto apro : aprobadores) {
                GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
                GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
                UsuarioDto usudto = gestor.getUsuario(apro.getId());
                Usuario usu = new Usuario(usudto);
                listaAprobadores.add(usu);
            }
            tbvAprobadores.getItems().addAll(listaAprobadores);
        }
    }

    private void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtAsunto, txtPasignada, txtSolicitante, dtpFapertura, dtpFcierre));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof DatePicker && ((DatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((DatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((DatePicker) node).getAccessibleText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }
    }

    private void bindGestion(Boolean nuevo) {
        if (!nuevo) {
            txtID.textProperty().bind(gestion.gesId);
        }
        txtAsunto.textProperty().bindBidirectional(gestion.gesAsunto);
        txtDescripcion.textProperty().bindBidirectional(gestion.gesDescripcion);
        dtpFapertura.valueProperty().bindBidirectional(gestion.gesFcreacion);
        dtpFcierre.valueProperty().bindBidirectional(gestion.gesFvencimiento);
    }

    private void unbindGestion() {
        txtID.textProperty().unbind();
        txtAsunto.textProperty().unbindBidirectional(gestion.gesAsunto);
        txtDescripcion.textProperty().unbindBidirectional(gestion.gesDescripcion);
        dtpFapertura.valueProperty().unbindBidirectional(gestion.gesFcreacion);
        dtpFcierre.valueProperty().unbindBidirectional(gestion.gesFvencimiento);
    }

    private void nuevaGestion() {
        if (AppContext.getInstance().get("Usuario") != null) {
            usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
            txtSolicitante.setText(usuario.getUsuNombre() + " " + usuario.getUsuPapellido() + " " + usuario.getUsuSapellido());
            txtSolicitante.editableProperty().set(false);
        }
        unbindGestion();
        gestion = new GestionDto();
        bindGestion(true);
        txtID.clear();
        txtID.requestFocus();
        smbActividad.setVisible(false);
        smbSubactividad.setVisible(false);
        tbvAprobadores.getItems().clear();
        tbvDocumentos.getItems().clear();
        documentos.clear();
        documentoDto = new DocumentoDto();
    }

    @FXML
    private void OnkeyPressesTxtID(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && txtID.getText() != null && !txtID.getText().isEmpty()) {
            cargarGestion(Long.valueOf(txtID.getText()));
        }
    }

    private void cargarGestion(Long id) {
        GestionService service = new GestionService();
        Respuesta respuesta = service.getGestion(id);
        if (respuesta.getEstado()) {
            gestion = (GestionDto) respuesta.getResultado("Gestion");
            AppContext.getInstance().set("Acti", gestion.getActividad());
            AppContext.getInstance().set("SubActi", gestion.getSubActividad());
            cargarDatos();
            cargarSolicitante();
            cargarAsignado();
            validarRequeridos();
            cargarTablaApro();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar gestion", getStage(), respuesta.getMensaje());
        }
    }

    private void cargarSolicitante() {
        txtSolicitante.clear();
        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        usuario = gestor.getUsuario(gestion.getGesSolicitante());
        if (usuario != null) {
            txtSolicitante.setText(usuario.getUsuNombre() + " " + usuario.getUsuPapellido() + " " + usuario.getUsuSapellido());
            txtSolicitante.editableProperty().set(false);
        }
    }

    private void cargarAsignado() {
        txtPasignada.clear();
        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        usuarioAsig = gestor.getUsuario(gestion.getGesPasignada());
        asignado = new Usuario(usuarioAsig);
        AppContext.getInstance().set("Asignado", asignado);
        if (usuario != null) {
            txtPasignada.setText(usuarioAsig.getUsuNombre() + " " + usuarioAsig.getUsuPapellido() + " " + usuarioAsig.getUsuSapellido());
            txtPasignada.editableProperty().set(false);
        }
    }

    private void cargarAreas() {
        AreaService service = new AreaService();
        Respuesta resp = service.getAreas();
        if (resp.getEstado()) {
            areas = (List<AreaDto>) resp.getResultado("Areas");
            List<MenuItem> items = new ArrayList<>();
            for (AreaDto area : areas) {
                items.add(new MenuItem(area.getNombre()));
            }
            smbAreas.getItems().addAll(items);
            items.forEach((t) -> {
                t.setOnAction((e) -> {
                    for (AreaDto area : areas) {
                        if (area.getNombre().equals(t.getText())) {
                            Respuesta r = service.getArea(area.getId());
                            are = (AreaDto) r.getResultado("Area");
                            AppContext.getInstance().set("Area", are);
                            if (are != null) {
                                cargarActividades(are);
                            }
                            smbActividad.setVisible(true);
                        }
                    }
                });
            });
        }
    }

    private void cargarActividades(AreaDto area) {
        actividades.clear();
        List<ActividadDto> acti = new ArrayList<>();
        ActividadService acts = new ActividadService();
        Respuesta res = acts.getActividades();
        if (res.getEstado()) {
            acti = (List<ActividadDto>) res.getResultado("Actividades");
            for (ActividadDto acti1 : acti) {
                res = acts.getActividad(acti1.getId());
                ActividadDto act = (ActividadDto) res.getResultado("Actividad");
                if (act.getArea().getId().equals(area.getId())) {
                    actividades.add(act);
                }
            }
        }
        List<MenuItem> items = new ArrayList<>();
        for (ActividadDto actividade : actividades) {
            items.add(new MenuItem(actividade.getNombre()));
        }
        smbActividad.getItems().clear();
        smbActividad.getItems().addAll(items);
        items.forEach((t) -> {
            t.setOnAction((e) -> {
                for (ActividadDto actividade : actividades) {
                    if (actividade.getNombre().equals(t.getText())) {
                        ActividadService as = new ActividadService();
                        Respuesta r = as.getActividad(actividade.getId());
                        actividad = (ActividadDto) r.getResultado("Actividad");
                        AppContext.getInstance().set("Acti", actividad);
                        if (actividad != null) {
                            cargarSubActividades();
                        }
                        smbSubactividad.setVisible(true);
                    }
                }
            });
        });
    }

    private void cargarSubActividades() {
        subActividades.clear();
        List<SubActividadDto> subacti = new ArrayList<>();
        SubActividadService subacts = new SubActividadService();
        Respuesta res = subacts.getSubActividades();
        if (res.getEstado()) {
            subacti = (List<SubActividadDto>) res.getResultado("SubActividades");
            for (SubActividadDto sub : subacti) {
                res = subacts.getSubActividad(sub.getId());
                SubActividadDto s = (SubActividadDto) res.getResultado("SubActividad");
                if (s.getActividad().getId().equals(actividad.getId())) {
                    subActividades.add(s);
                }
            }
        }
        List<MenuItem> items = new ArrayList<>();
        for (SubActividadDto subact : subActividades) {
            items.add(new MenuItem(subact.getNombre()));
        }
        smbSubactividad.getItems().addAll(items);
        items.forEach((t) -> {
            t.setOnAction((e) -> {
                for (SubActividadDto subact : subActividades) {
                    if (subact.getNombre().equals(t.getText())) {
                        gestion.setSubActividad(subact);
                        AppContext.getInstance().set("SubActi", subact);
                    }
                }
            });
        });
    }

    @FXML
    private void LimpiarRegistro(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar gestion", getStage(), "¿Esta seguro que desea limpiar el registro?")) {
            nuevaGestion();
            txtPasignada.clear();
            txtPasignada.editableProperty().set(true);
            resetear();
            AppContext.getInstance().delete("Area");
            AppContext.getInstance().delete("Acti");
            AppContext.getInstance().delete("SubActi");
            AppContext.getInstance().delete("Asignado");
            AppContext.getInstance().delete("Gestion");
        }
    }

    @FXML
    private void EliminarGestion(ActionEvent event) {
    }

    @FXML
    private void GuardarGestiones(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar gestion", getStage(), invalidos);
            } else {
                guardarCambios();
                guardarDatos();
                resetear();
                if (gestion.getAprobadores().isEmpty()) {
                    gestion.setGesEstado("C");
                }
                GestionService service = new GestionService();
                Respuesta respuesta = service.guardarGestion(gestion);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar gestion", getStage(), respuesta.getMensaje());
                } else {
                    unbindGestion();
                    gestion = (GestionDto) respuesta.getResultado("Gestion");
                    if (gestion.getGesVersion() == 1) {
                        guardarDoc();
                        guardarSeguimiento();
                        notificarSolicitante();
                        notificarAsignado();
                        bindGestion(false);
                    } else {
                        guardarDoc();
                        generarSeguimiento();
                        notificarSeguimiento();
                    }
                    cargarTablaApro();
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar gestion", getStage(), "Gestion actualizada correctamente.");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(GestionesController.class
                    .getName()).log(Level.SEVERE, "Error guardando la gestion.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar gestion", getStage(), "Ocurrio un error guardando la gestion.");
        }
    }

    private void guardarSeguimiento() {
        seguimiento.setGestion(gestion);
        seguimiento.setSegDetalle("La gestion ha sido registrada satisfactoriamente");
        seguimiento.setSegFseguimiento(LocalDate.now());
        seguimiento.setSegIdusuario(usuario.getUsuId());
        SeguimientoService service = new SeguimientoService();
        Respuesta res = service.guardarSeguimiento(seguimiento);
    }

    private void generarSeguimiento() {
        seguimiento.setGestion(gestion);
        seguimiento.setSegDetalle("Cambio en la gestion. \n" + cambio);
        seguimiento.setSegFseguimiento(LocalDate.now());
        seguimiento.setSegIdusuario(usuario.getUsuId());
        SeguimientoService service = new SeguimientoService();
        Respuesta res = service.guardarSeguimiento(seguimiento);
    }

    private void notificarSolicitante() {
        GestionService service = new GestionService();
        Respuesta res = service.notificar(usuario.getUsuCorreo(), "La gestion que solicito ha sido registrada "
                + "satisfactoriamente");

    }

    private void notificarAsignado() {
        GestionService service = new GestionService();
        Respuesta res = service.notificar(asignado.getUsuCorreo(), "Estimado, " + asignado.getUsuNombre() + ". Se le ha asignado una gestion de "
                + solicitante.getUsuNombre());
    }

    private void notificarAprobador() {
        GestionService service = new GestionService();
        Respuesta res = service.notificar(aprobador.getUsuCorreo(), "Estimado, " + aprobador.getUsuNombre() + ". El usuario " + solicitante.getUsuNombre()
                + " ha solicitado que apruebe una gestion. ");
    }

    private void notificarSeguimiento() {
        GestionService service = new GestionService();
        Respuesta res = service.notificar(solicitante.getUsuCorreo(), "Se ha realizado un seguimiento a la gestion "
                + gestion.getGesAsunto());
    }

    @FXML
    private void BuscarAsignados(ActionEvent event) {
        if (gestion.getGesId() != null) {
            AppContext.getInstance().set("Id", gestion.getGesId());
        }
        AppContext.getInstance().set("Asunto", gestion.gesAsunto);
        AppContext.getInstance().set("Descripcion", gestion.gesDescripcion);
        AppContext.getInstance().set("FechaC", gestion.gesFcreacion);
        AppContext.getInstance().set("FechaV", gestion.gesFvencimiento);
        FlowController.getInstance().goViewInWindow("EmpleadosxArea");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void BuscarAprobadores(ActionEvent event) {
        if (gestion.getGesId() != null) {
            AppContext.getInstance().set("Id", gestion.getGesId());
        }
        AppContext.getInstance().set("Asunto", gestion.gesAsunto);
        AppContext.getInstance().set("Descripcion", gestion.gesDescripcion);
        AppContext.getInstance().set("FechaC", gestion.gesFcreacion);
        AppContext.getInstance().set("FechaV", gestion.gesFvencimiento);
        FlowController.getInstance().goViewInWindow("BuscarAprobadores");
    }

    @Override
    public void initialize() {
    }

    private void guardarCambios() {
        if (!gestion.getGesAsunto().equals(txtAsunto.getText())) {
            cambio = "Nuevo asunto: " + txtAsunto.getText();
        } else if (!gestion.getGesDescripcion().equals(txtDescripcion.getText())) {
            cambio = "Nueva descripcion: " + txtDescripcion.getText();
        } else if (!gestion.getGesFvencimiento().equals(dtpFcierre.getValue())) {
            cambio = "Nueva fecha de vencimiento: " + dtpFcierre.getValue().toString();
        }
    }

    private void cargarDatos() {
        txtAsunto.setText(gestion.getGesAsunto());
        txtDescripcion.setText(gestion.getGesDescripcion());
        dtpFapertura.setValue(gestion.getGesFcreacion());
        dtpFcierre.setValue(gestion.getGesFvencimiento());
    }

    private void guardarDatos() {
        gestion.setGesAsunto(txtAsunto.getText());
        gestion.setGesDescripcion(txtDescripcion.getText());
        gestion.setGesFcreacion(dtpFapertura.getValue());
        gestion.setGesFvencimiento(dtpFcierre.getValue());
        gestion.setGesSolicitante(usuario.getUsuId());
        gestion.setGesPasignada(asignado.getUsuId());
        if (AppContext.getInstance().get("Id") != null) {
            Long id = (Long) AppContext.getInstance().get("Id");
            gestion.setGesId(id);
        }
        gestion.setActividad((ActividadDto) AppContext.getInstance().get("Acti"));
        gestion.setSubActividad((SubActividadDto) AppContext.getInstance().get("SubActi"));
        if (!gestion.getAprobadores().isEmpty()) {
            for (AprobadorDto aprobadore : aprobadores) {
                for (AprobadorDto apro : gestion.getAprobadores()) {
                    if (!aprobadore.getId().equals(apro.getId())) {
                        aprobadore.setModificado(true);
                        gestion.getAprobadores().add(aprobadore);
                        gestion.setGesEstado("P");
                    }
                }
            }
        } else {
            for (AprobadorDto aprobadore : aprobadores) {
                aprobadore.setModificado(true);
                GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
                GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
                aprobador = gestor.getUsuario(aprobadore.getId());
                gestion.getAprobadores().add(aprobadore);
                gestion.setGesEstado("P");
                notificarAprobador();
            }
        }
    }

    private void guardarAprobadores() {
        if (AppContext.getInstance().get("ListApro") == null) {
            if (AppContext.getInstance().get("Aprobador") != null) {
                aprobadores.add((AprobadorDto) AppContext.getInstance().get("Aprobador"));
                AppContext.getInstance().set("ListApro", aprobadores);
            }
        } else {
            aprobadores.addAll((Collection<? extends AprobadorDto>) AppContext.getInstance().get("ListApro"));
            aprobadores.add((AprobadorDto) AppContext.getInstance().get("Aprobador"));
            AppContext.getInstance().set("ListApro", aprobadores);
        }
    }

    private void restaurarInfo() {
        if (AppContext.getInstance().get("Asunto") != null) {
            txtAsunto.textProperty().bindBidirectional((Property<String>) AppContext.getInstance().get("Asunto"));
        }
        if (AppContext.getInstance().get("Descripcion") != null) {
            txtDescripcion.textProperty().bindBidirectional((Property<String>) AppContext.getInstance().get("Descripcion"));
        }
        if (AppContext.getInstance().get("FechaC") != null) {
            dtpFapertura.valueProperty().bindBidirectional((Property<LocalDate>) AppContext.getInstance().get("FechaC"));
        }
        if (AppContext.getInstance().get("FechaV") != null) {
            dtpFcierre.valueProperty().bindBidirectional((Property<LocalDate>) AppContext.getInstance().get("FechaV"));
        }
        if (AppContext.getInstance().get("Acti") != null) {
            ActividadDto act = (ActividadDto) AppContext.getInstance().get("Acti");
            smbActividad.setText(act.getNombre());
            smbActividad.setVisible(true);
        }
        if (AppContext.getInstance().get("Area") != null) {
            AreaDto are = (AreaDto) AppContext.getInstance().get("Area");
            smbAreas.setText(are.getNombre());
        }
        if (AppContext.getInstance().get("SubActi") != null) {
            SubActividadDto sub = (SubActividadDto) AppContext.getInstance().get("SubActi");
            smbSubactividad.setText(sub.getNombre());
            smbSubactividad.setVisible(true);
        }
    }

    private void resetear() {
        AppContext.getInstance().delete("ListApro");
        aprobadores.clear();
        AppContext.getInstance().delete("FechaV");
        AppContext.getInstance().delete("FechaC");
        AppContext.getInstance().delete("Descripcion");
        AppContext.getInstance().delete("Asunto");
        AppContext.getInstance().delete("Id");
    }

    private void cargarTablaApro() {
        List<Usuario> listaAprobadores = new ArrayList<>();
        tbvAprobadores.getItems().clear();
        for (AprobadorDto apro : gestion.getAprobadores()) {
            GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
            GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
            UsuarioDto usudto = gestor.getUsuario(apro.getId());
            Usuario usu = new Usuario(usudto);
            listaAprobadores.add(usu);
        }
        tbvAprobadores.getItems().addAll(listaAprobadores);
    }

    @FXML
    private void AdjuntarDoc(ActionEvent event) throws FileNotFoundException, IOException {

        FileChooser fileChooser = new FileChooser();
        List<File> selectedFiles = fileChooser.showOpenMultipleDialog(null);
        InputStream in = new FileInputStream(selectedFiles.get(0));
        documentoDto = new DocumentoDto();
        documentoDto.setDocArchivo(in.readAllBytes());
        documentoDto.setGestion(gestion);
        documentos.add(documentoDto);
        tbvDocumentos.getItems().addAll(documentos);
    }

    private void guardarDoc() {
        if (documentoDto != null) {
            DocumentoService service = new DocumentoService();
            Respuesta respuesta = service.guardarDocumento(documentoDto);
            documentoDto = (DocumentoDto) respuesta.getResultado("Documento");
        }

    }

    @FXML
    private void BuscarGestion(ActionEvent event) {
        FlowController.getInstance().goView("BuscarGestiones");
        FlowController.getInstance().initialize();
    }

    private class ButtonCell extends TableCell<Usuario, Boolean> {

        final Button cellButton = new Button();

        ButtonCell() {
            cellButton.setPrefWidth(300);
            cellButton.getStyleClass().add("jfx-btnimg-tbveliminar");
            cellButton.setOnAction((event) -> {
                Usuario usu = ButtonCell.this.getTableView().getItems().get(ButtonCell.this.getIndex());
                tbvAprobadores.getItems().remove(usu);
                if (aprobadores.size() == 1) {
                    aprobadores.clear();
                } else {
                    aprobadores.remove(tbvAprobadores.getItems().indexOf(usu));
                }
                AppContext.getInstance().set("ListApro", aprobadores);
                tbvAprobadores.refresh();
            });

        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty); //To change body of generated methods, choose Tools | Templates.
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    public class BtnCell extends TableCell<DocumentoDto, Boolean> {

        final Button cellButton = new Button();

        BtnCell() {
            cellButton.setPrefWidth(400);
            cellButton.getStyleClass().add("jfx-btnimg-visualizar");
            cellButton.setOnAction((event) -> {
                DocumentoDto doc = BtnCell.this.getTableView().getItems().get(BtnCell.this.getIndex());
                tbvDocumentos.getItems().remove(doc);
                if (documentos.size() == 1) {
                    documentos.clear();
                } else {
                    documentos.remove(tbvDocumentos.getItems().indexOf(doc));
                }
                tbvDocumentos.refresh();
            });

        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty); //To change body of generated methods, choose Tools | Templates.
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

}
