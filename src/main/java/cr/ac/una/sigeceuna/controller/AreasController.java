package cr.ac.una.sigeceuna.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorseguridad.model.Usuario;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.AreaDto;
import cr.ac.una.sigeceuna.model.EmpleadoDto;
import cr.ac.una.sigeceuna.service.AreaService;
import cr.ac.una.sigeceuna.service.EmpleadoService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Formato;
import cr.ac.una.sigeceuna.util.Mensaje;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class AreasController extends Controller implements Initializable {

    @FXML
    private TabPane tbpAreaEmple;
    @FXML
    private Tab tbArea;
    @FXML
    private JFXTextField txtID;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXCheckBox chkActivo;
    @FXML
    private TableView<AreaDto> tbvAreas;
    @FXML
    private TableColumn<AreaDto, String> tbcID;
    @FXML
    private TableColumn<AreaDto, String> tbcNombre;
    @FXML
    private Tab tbAgregarEmpleados;
    @FXML
    private JFXTextField txtIdEmple;
    @FXML
    private JFXTextField txtNombreEmple;
    @FXML
    private JFXButton btnBuscarEmple;
    @FXML
    private TableView<Usuario> tbvEmpleados;
    @FXML
    private TableColumn<Usuario, String> tbcIDEmple;
    @FXML
    private TableColumn<Usuario, String> tbcNombreEmple;
    @FXML
    private TableColumn<Usuario, Boolean> tbcEliminarEmple;
    @FXML
    private TableColumn<Usuario, String> tbcPapellido;
    @FXML
    private TableColumn<Usuario, String> tbcSapellido;
    @FXML
    private TableColumn<Usuario, String> tbcCedula;
    @FXML
    private TableColumn<Usuario, String> tbcTelefono;
    @FXML
    private JFXButton btnAgregarEmple;
    @FXML
    private JFXButton btnNuevo;
    @FXML
    private JFXButton btnEliminar;
    @FXML
    private JFXButton btnGuardar;

    private AreaDto areaDto;
    private EmpleadoDto empleadoDto;
    private UsuarioDto usuarioDto;
    private Usuario usuario;

    List<Node> requeridos = new ArrayList<>();

    List<AreaDto> listaTabla = new ArrayList<>();

    List<Usuario> listaEmpleados = new ArrayList<>();

    List<EmpleadoDto> empleados = new ArrayList<>();
    @FXML
    private ImageView imgvIcono;
    @FXML
    private JFXButton btnIcono;
    
    Image img;
    Image im = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtID.setTextFormatter(Formato.getInstance().integerFormat());
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(30));
        areaDto = new AreaDto();
        nuevoArea();
        indicarRequeridos();
        tablaLista();

        tbcID.setCellValueFactory(cd -> cd.getValue().id);
        tbcNombre.setCellValueFactory(cd -> cd.getValue().nombre);

        tbcIDEmple.setCellValueFactory(cd -> cd.getValue().usuId);
        tbcNombreEmple.setCellValueFactory(cd -> cd.getValue().usuNombre);
        tbcCedula.setCellValueFactory(cd -> cd.getValue().usuCedula);
        tbcPapellido.setCellValueFactory(cd -> cd.getValue().usuPApellido);
        tbcSapellido.setCellValueFactory(cd -> cd.getValue().usuSApellido);
        tbcTelefono.setCellValueFactory(cd -> cd.getValue().usuCelular);

        tbcEliminarEmple.setCellValueFactory((TableColumn.CellDataFeatures< Usuario, Boolean> p)
                -> new SimpleBooleanProperty(p.getValue() != null));

        tbcEliminarEmple.setCellFactory((TableColumn<Usuario, Boolean> p) -> new ButtonCell());

        tbvAreas.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends AreaDto> observable, AreaDto oldValue, AreaDto newValue) -> {
            unbindArea();
            if (newValue != null) {
                AppContext.getInstance().delete("Empleado");
                listaEmpleados.clear();
                areaDto = newValue;
                AreaService areaService = new AreaService();
                Respuesta respuesta = areaService.getArea(areaDto.getId());
                areaDto = (AreaDto) respuesta.getResultado("Area");
                ByteArrayInputStream byt = new ByteArrayInputStream(areaDto.getLogo());
                Image im = new Image(byt);
                imgvIcono.setImage(im);
                bindArea(false);
                
                AppContext.getInstance().set("Are", areaDto);

                tbvEmpleados.getItems().clear();
                for (EmpleadoDto empleado : areaDto.getEmpleados()) {
                    GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
                    GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
                    UsuarioDto usudto = gestor.getUsuario(empleado.getId());
                    Usuario usu = new Usuario(usudto);
                    listaEmpleados.add(usu);
                }
                tbvEmpleados.getItems().addAll(listaEmpleados);
            }
        });
        
        img = (Image) AppContext.getInstance().get("Imagen");
        imgvIcono.setImage(img);

        empleadoDto = new EmpleadoDto();
        usuarioDto = new UsuarioDto();

        if (AppContext.getInstance().get("Empleado") != null) {
            areaDto = (AreaDto) AppContext.getInstance().get("Are");
            usuario = (Usuario) AppContext.getInstance().get("Empleado");
            cargarEmpleado(usuario.getUsuId());
            listaEmpleados = (List<Usuario>) AppContext.getInstance().get("ListEmple");
            tbvEmpleados.getItems().addAll(listaEmpleados);
            tbpAreaEmple.getSelectionModel().selectNext();
        }
        
    }

    public void tablaLista() {
        AreaService areaService = new AreaService();
        Respuesta r = areaService.getAreas();
        if (r.getEstado()) {
            listaTabla = (List<AreaDto>) r.getResultado("Areas");
            tbvAreas.getItems().clear();
            tbvAreas.getItems().addAll(listaTabla);
            tbvAreas.refresh();
        }
    }

    public void indicarRequeridos() {
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtNombre));
    }

    public String validarRequeridos() {
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos) {
            if (node instanceof JFXTextField && !((JFXTextField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXTextField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXTextField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXPasswordField && !((JFXPasswordField) node).validate()) {
                if (validos) {
                    invalidos += ((JFXPasswordField) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXPasswordField) node).getPromptText();
                }
                validos = false;
            } else if (node instanceof JFXDatePicker && ((JFXDatePicker) node).getValue() == null) {
                if (validos) {
                    invalidos += ((JFXDatePicker) node).getAccessibleText();
                } else {
                    invalidos += "," + ((JFXDatePicker) node).getAccessibleText();
                }
                validos = false;
            } else if (node instanceof JFXComboBox && ((JFXComboBox) node).getSelectionModel().getSelectedIndex() < 0) {
                if (validos) {
                    invalidos += ((JFXComboBox) node).getPromptText();
                } else {
                    invalidos += "," + ((JFXComboBox) node).getPromptText();
                }
                validos = false;
            }
        }
        if (validos) {
            return "";
        } else {
            return "Campos requeridos o con problemas de formato [" + invalidos + "].";
        }
    }

    private void bindArea(Boolean nuevo) {
        if (!nuevo) {
            txtID.textProperty().bind(areaDto.id);
        }
        txtNombre.textProperty().bindBidirectional(areaDto.nombre);
        chkActivo.selectedProperty().bindBidirectional(areaDto.estado);
        byte[] imagen = (byte[]) AppContext.getInstance().get("Icono");
        areaDto.setLogo(imagen);
    }

    private void unbindArea() {
        txtID.textProperty().unbind();
        txtNombre.textProperty().unbindBidirectional(areaDto.nombre);
        chkActivo.selectedProperty().unbindBidirectional(areaDto.estado);
    }

    private void nuevoArea() {
        unbindArea();
        areaDto = new AreaDto();
        bindArea(true);
        txtID.clear();
        txtID.requestFocus();
        imgvIcono.setImage(im);
    }

    @FXML
    private void onKeyPressedTxtID(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtID.getText().isEmpty()) {
            cargarArea(Long.valueOf(txtID.getText()));
        }
    }

    private void cargarArea(Long id) {
        AreaService service = new AreaService();
        Respuesta respuesta = service.getArea(id);

        if (respuesta.getEstado()) {
            unbindArea();
            areaDto = (AreaDto) respuesta.getResultado("Area");
            ByteArrayInputStream byt = new ByteArrayInputStream(areaDto.getLogo());
            Image im = new Image(byt);
            imgvIcono.setImage(im);
            bindArea(false);
            validarRequeridos();
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar Area", getStage(), respuesta.getMensaje());
        }
    }

    @FXML
    private void onActionBtnNuevo(ActionEvent event) {
        if (tbAgregarEmpleados.isSelected()) {
            nuevoEmpleado();
        } else if (tbArea.isSelected()) {
            if (new Mensaje().showConfirmation("Limpiar Area", getStage(), "¿Esta seguro que desea limpiar el registro?")) {
                nuevoArea();
            }
        }
    }

    @FXML
    private void onActionBtnEliminar(ActionEvent event) {
        try {
            if (areaDto.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Area", getStage(), "Debe cargar el area que desea eliminar.");
            } else {

                AreaService service = new AreaService();
                Respuesta respuesta = service.eliminarArea(areaDto.getId());
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Area", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar Area", getStage(), "Area eliminado correctamente.");
                    nuevoArea();
                    tablaLista();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(AreasController.class.getName()).log(Level.SEVERE, "Error eliminando el area.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar Area", getStage(), "Ocurrio un error eliminando el area.");
        }
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
        try {
            String invalidos = validarRequeridos();
            if (!invalidos.isEmpty()) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Area", getStage(), invalidos);
            } else {
                AreaService service = new AreaService();
                Respuesta respuesta = service.guardarArea(areaDto);
                if (!respuesta.getEstado()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Area", getStage(), respuesta.getMensaje());
                } else {
                    unbindArea();
                    areaDto = (AreaDto) respuesta.getResultado("Area");
                    bindArea(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar Area", getStage(), "Area actualizada correctamente.");
                    tablaLista();
                    AppContext.getInstance().delete("Empleado");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(AreasController.class.getName()).log(Level.SEVERE, "Error guardando el area.", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar Area", getStage(), "Ocurrio un error guardando el area.");
        }
    }

    @FXML
    private void onSelectionChangedTbEmpleados(Event event) {
        if (tbAgregarEmpleados.isSelected()) {
            if (areaDto.getId() == null) {
                new Mensaje().showModal(Alert.AlertType.ERROR, "Area", getStage(), "Debe cargar un area a la que desea agregar un empleado.");
                tbpAreaEmple.getSelectionModel().select(tbArea);
            }
        } else if (tbArea.isSelected()) {

        }
    }

    private void nuevoEmpleado() {
        tbvEmpleados.getSelectionModel().select(null);
        empleadoDto = new EmpleadoDto();
        usuario = new Usuario();
        txtIdEmple.clear();
        txtIdEmple.requestFocus();
        txtNombreEmple.clear();
    }

    @FXML
    private void onKeyPressedTxtIDEmple(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtIdEmple.getText().isEmpty()) {
            cargarEmpleado(Long.valueOf(txtIdEmple.getText()));
        }
    }

    private void cargarEmpleado(Long id) {
        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        usuarioDto = gestor.getUsuario(id);
        if (usuarioDto != null) {
            usuario = new Usuario(usuarioDto);
            txtIdEmple.setText(usuarioDto.getUsuId().toString());
            txtNombreEmple.setText(usuarioDto.getUsuNombre() + " " + usuarioDto.getUsuPapellido());
            EmpleadoService service1 = new EmpleadoService();
            Respuesta res = service1.getEmpleado(id);
            if (res.getEstado()) {
                empleadoDto = (EmpleadoDto) res.getResultado("Empleado");
            }
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar empleado", getStage(),
                    "No existe un empleado con el codigo ingresado");
        }
    }

    @FXML
    private void onActionBtnBuscarEmple(ActionEvent event) {
        AppContext.getInstance().set("ListEmple", listaEmpleados);
        FlowController.getInstance().goViewInWindow("BuscarEmpleados");
    }

    @FXML
    private void onActionBtnAgregarEmple(ActionEvent event) {
        if (usuario == null || usuario.getUsuNombre().isEmpty()) {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Agregar Empleado", getStage(),
                    "Es necesario cargar un empleado para agregarlo a la lista.");
        } else if (tbvEmpleados.getItems() == null || !tbvEmpleados.getItems().stream().
                anyMatch(a -> a.usuId.get().equalsIgnoreCase(usuario.usuId.toString()))) {
            empleadoDto.setModificado(true);
            tbvEmpleados.getItems().add(usuario);
            areaDto.getEmpleados().add(empleadoDto);
            tbvEmpleados.refresh();
        }
        nuevoEmpleado();
    }

    @FXML
    private void onActionBtnIcono(ActionEvent event) {
        FlowController.getInstance().goViewInWindow("Iconos");

    }

    private class ButtonCell extends TableCell<Usuario, Boolean> {

        final Button cellButton = new Button();

        ButtonCell() {
            cellButton.setPrefWidth(500);
            cellButton.getStyleClass().add("jfx-btnimg-eliminar");
            cellButton.setOnAction((event) -> {
                Usuario usu = ButtonCell.this.getTableView().getItems().get(ButtonCell.this.getIndex());
                EmpleadoService empleadoService = new EmpleadoService();
                Respuesta res = empleadoService.getEmpleado(usu.getUsuId());
                EmpleadoDto emp = (EmpleadoDto) res.getResultado("Empleado");
                areaDto.getEmpleadosEliminados().add(emp);
                tbvEmpleados.getItems().remove(usu);
                tbvEmpleados.refresh();
            });

        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty); //To change body of generated methods, choose Tools | Templates.
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    @Override
    public void initialize() {

    }

}
