/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.RolDto;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.AprobadorDto;
import cr.ac.una.sigeceuna.model.EmpleadoDto;
import cr.ac.una.sigeceuna.service.AprobadorService;
import cr.ac.una.sigeceuna.service.EmpleadoService;
import cr.ac.una.sigeceuna.util.AppContext;
import cr.ac.una.sigeceuna.util.FlowController;
import cr.ac.una.sigeceuna.util.Mensaje;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author JosueNG
 */
public class PrincipalController extends Controller implements Initializable {

    @FXML
    private JFXButton btnPrincipal;
    @FXML
    private JFXButton btnCrearGestion;
    @FXML
    private JFXButton btnAreas;
    @FXML
    private JFXButton btnActividades;
    @FXML
    private JFXButton btnCalendario;
    @FXML
    private JFXButton btnReportes;
    @FXML
    private JFXButton btnConfiguracion;
    @FXML
    private JFXButton btnCerrarSesion;
    @FXML
    private Label lblNombre;

    List<UsuarioDto> usuarios;
    List<EmpleadoDto> empleados;
    List<AprobadorDto> aprobadorDtos;
    UsuarioDto usuario;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usuario = (UsuarioDto) AppContext.getInstance().get("Usuario");
        lblNombre.setText(usuario.getUsuNombre() + " " + usuario.getUsuPapellido());
        RolDto rol = (RolDto) AppContext.getInstance().get("Rol");
        if (rol.equals("Solicitante")) {
            btnActividades.setDisable(true);
            btnAreas.setDisable(true);
            btnReportes.setDisable(true);
            btnConfiguracion.setDisable(true);
        }
        if (rol.equals("Gestor")) {
            btnActividades.setDisable(true);
            btnAreas.setDisable(true);
        }
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void onActionbtnPrincipal(ActionEvent event) {
        FlowController.getInstance().goView("Personal");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onActionbtnCrearGestion(ActionEvent event) {
        AppContext.getInstance().delete("Area");
        AppContext.getInstance().delete("Acti");
        AppContext.getInstance().delete("SubActi");
        AppContext.getInstance().delete("Asignado");
        AppContext.getInstance().delete("Gestion");
        AppContext.getInstance().delete("ListApro");
        AppContext.getInstance().delete("Aprobador");
        FlowController.getInstance().goView("Gestiones");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onActionbtnAreas(ActionEvent event) {
        AppContext.getInstance().delete("Are");
        AppContext.getInstance().delete("Empleado");
        AppContext.getInstance().delete("ListEmple");
        FlowController.getInstance().goView("Areas");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onActionbtnActividades(ActionEvent event) {
        if (AppContext.getInstance().get("Are") != null) {
            FlowController.getInstance().goView("ActiSubActi");
            FlowController.getInstance().initialize();
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Actividades", getStage(),
                    "Debe seleccionar un area para registrarle las actividades");
        }

    }

    @FXML
    private void onActionbtnCalendario(ActionEvent event) {
        FlowController.getInstance().goView("CalendarioM2");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onActionbtnReportes(ActionEvent event) {
        FlowController.getInstance().goView("Reportes");
        FlowController.getInstance().initialize();
    }

    @FXML
    private void onActionbtnConfiguracion(ActionEvent event) {
    }

    @FXML
    private void onActionbtnCerrarSesion(ActionEvent event) {
        FlowController.getInstance().salir();
        FlowController.getInstance().goViewInWindow("LogIn");
    }

}
