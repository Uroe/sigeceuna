/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.sigeceuna.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs;
import cr.ac.una.gestorseguridad.ws.GestorSeguridadWs_Service;
import cr.ac.una.gestorseguridad.ws.UsuarioDto;
import cr.ac.una.sigeceuna.model.AreaDto;
import cr.ac.una.sigeceuna.model.EmpleadoDto;
import cr.ac.una.sigeceuna.service.AreaService;
import cr.ac.una.sigeceuna.service.EmpleadoService;
import cr.ac.una.sigeceuna.service.ReporteService;
import cr.ac.una.sigeceuna.util.Formato;
import cr.ac.una.sigeceuna.util.Mensaje;
import cr.ac.una.sigeceuna.util.VisorReportes;
import cr.ac.una.sigeceuna.util.Respuesta;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.swing.JRViewer;

/**
 * FXML Controller class
 *
 * @author Lesmi
 */
public class ReportesController extends Controller implements Initializable {

    @FXML
    private TabPane tbpReportes;
    @FXML
    private Tab tabEmpleados;
    @FXML
    private JFXTextField txtId;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtArea;
    @FXML
    private DatePicker dtpFinicial;
    @FXML
    private DatePicker dtpffinal;
    @FXML
    private JFXButton btnGenerar;
    @FXML
    private Tab tabAreas;
    @FXML
    private HBox btnNuevo;
    @FXML
    private JFXButton btnGenReportesAre;
    @FXML
    private SplitMenuButton smbAreas;
    @FXML
    private JFXButton btnGenReportesEmp;

    List<AreaDto> areas = new ArrayList<>();
    UsuarioDto usuario;
    boolean sel = false;
    AreaDto area;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtId.setTextFormatter(Formato.getInstance().integerFormat());
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(50));
        txtArea.setTextFormatter(Formato.getInstance().letrasFormat(50));
        usuario = new UsuarioDto();
        List<MenuItem> items = new ArrayList<>();
        AreaService service = new AreaService();
        Respuesta respuesta = service.getAreas();
        if (respuesta.getEstado()) {
            areas = (List<AreaDto>) respuesta.getResultado("Areas");
            for (AreaDto area1 : areas) {
                items.add(new MenuItem(area1.getNombre()));
            }
            smbAreas.getItems().addAll(items);
        }
        items.forEach((t) -> {
            t.setOnAction((e) -> {
                for (AreaDto area1 : areas) {
                    if (area1.getNombre().equals(t.getText())) {
                        area = area1;
                        sel = true;
                        smbAreas.setText(area1.getNombre());
                    }
                }
            });
        });

    }

    private void cargarEmpleado(Long id) {
        GestorSeguridadWs_Service service = new GestorSeguridadWs_Service();
        GestorSeguridadWs gestor = service.getGestorSeguridadWsPort();
        usuario = gestor.getUsuario(id);
        if (usuario != null) {
            txtId.setText(usuario.getUsuId().toString());
            txtNombre.setText(usuario.getUsuNombre() + " " + usuario.getUsuPapellido() + " " + usuario.getUsuSapellido());
            EmpleadoService service1 = new EmpleadoService();
            Respuesta res = service1.getEmpleado(id);
            if (res.getEstado()) {
                EmpleadoDto emp = (EmpleadoDto) res.getResultado("Empleado");
//                txtArea.setText(emp.getAreas().get(0).getNombre());
            }
            sel = true;
        } else {
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar empleado", getStage(),
                    "No existe un empleado con el codigo ingresado");
        }
    }

    @Override
    public void initialize() {

    }

    @FXML
    private void onKeyPressedTxtId(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER && !txtId.getText().isEmpty()) {
            cargarEmpleado(Long.valueOf(txtId.getText()));
        }
    }

    @FXML
    private void BuscarEmpleado(ActionEvent event) {
    }

    @FXML
    private void GenerarReporte(ActionEvent event) {
        if (sel) {
            ReporteService service = new ReporteService();
            Respuesta respuesta = service.getReporte(usuario.getUsuId(), fechaInicial(), fechafinal());
            if (respuesta.getEstado()) {
                String xml = (String) respuesta.getResultado("Reporte");
                Base64.Decoder decoder = Base64.getDecoder();
                File file = new File("Reporte.xml");
                byte[] fileArray = xml.getBytes(StandardCharsets.UTF_8);
                String xmlBase64 = Base64.getEncoder().encodeToString(fileArray);
                byte[] xmlBytesDecoded = Base64.getDecoder().decode(xmlBase64);
                try {
                    OutputStream out = new FileOutputStream(file);
                    out.write(xmlBytesDecoded);
                    out.close();
                    InputStream in = new FileInputStream(file);
                    in.read(xmlBytesDecoded);
                    in.close();
                    JRViewer v = new JRViewer("Reporte.xml", true);
                    VisorReportes panel = new VisorReportes();
                    panel.visor(v);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ReportesController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException | JRException ex) {
                    Logger.getLogger(ReportesController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Generar reporte", getStage(),
                    "Debe cargar un empleado");
        }
    }

    private String fechaInicial() {
        String finicial = dtpFinicial.getValue().getDayOfMonth() + "/" + dtpFinicial.getValue().getMonthValue()
                + "/" + dtpFinicial.getValue().getYear();
        return finicial;
    }

    private String fechafinal() {
        String ffinal = dtpffinal.getValue().getDayOfMonth() + "/" + dtpffinal.getValue().getMonthValue()
                + "/" + dtpffinal.getValue().getYear();
        return ffinal;
    }

    @FXML
    private void NuevoEmpleado(MouseEvent event) {
        txtNombre.clear();
        txtId.clear();
        txtId.requestFocus();
    }

    @FXML
    private void onActionbtnGenReportesAre(ActionEvent event) {
        ReporteService service = new ReporteService();
        Respuesta respuesta = service.getReporteAreas();
        if (respuesta.getEstado()) {
            String xml = (String) respuesta.getResultado("Reporte");
            Base64.Decoder decoder = Base64.getDecoder();
            File file = new File("ReporteAreas.xml");
            byte[] fileArray = xml.getBytes(StandardCharsets.UTF_8);
            String xmlBase64 = Base64.getEncoder().encodeToString(fileArray);
            byte[] xmlBytesDecoded = Base64.getDecoder().decode(xmlBase64);
            try {
                OutputStream out = new FileOutputStream(file);
                out.write(xmlBytesDecoded);
                out.close();
                InputStream in = new FileInputStream(file);
                in.read(xmlBytesDecoded);
                in.close();
                JRViewer v = new JRViewer("ReporteAreas.xml", true);
                VisorReportes panel = new VisorReportes();
                panel.visor(v);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReportesController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException | JRException ex) {
                Logger.getLogger(ReportesController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    private void onActionbtnGenReportesEmp(ActionEvent event) {
        if (sel) {
            ReporteService service = new ReporteService();
            Respuesta respuesta = service.getReporteEmpleados(area.getId());
            if (respuesta.getEstado()) {
                String xml = (String) respuesta.getResultado("Reporte");
                Base64.Decoder decoder = Base64.getDecoder();
                File file = new File("ReporteEmpleados.xml");
                byte[] fileArray = xml.getBytes(StandardCharsets.UTF_8);
                String xmlBase64 = Base64.getEncoder().encodeToString(fileArray);
                byte[] xmlBytesDecoded = Base64.getDecoder().decode(xmlBase64);
                try {
                    OutputStream out = new FileOutputStream(file);
                    out.write(xmlBytesDecoded);
                    out.close();
                    InputStream in = new FileInputStream(file);
                    in.read(xmlBytesDecoded);
                    in.close();
                    JRViewer v = new JRViewer("ReporteEmpleados.xml", true);
                    VisorReportes panel = new VisorReportes();
                    panel.visor(v);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ReportesController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException | JRException ex) {
                    Logger.getLogger(ReportesController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            new Mensaje().showModal(Alert.AlertType.INFORMATION, "Generar reporte", getStage(),
                    "Debe seleccionar un area");
        }
    }

}
