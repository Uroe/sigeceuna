package cr.ac.una.sigeceuna;

import cr.ac.una.sigeceuna.util.FlowController;
import javafx.application.Application;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.scene.image.Image;

/**
 * JavaFX App
 */
public class App extends Application {
    
    @Override
    public void start(Stage stage) throws IOException {
        FlowController.getInstance().InitializeFlow(stage,ResourceBundle.getBundle("/cr/ac/una/sigeceuna/idiomas/Espanol"));
        FlowController.getInstance().InitializeFlow(stage, null);
        stage.getIcons().add(new Image(App.class.getResourceAsStream("/cr/ac/una/sigeceuna/resources/Usuario.png")));
        stage.setTitle("SigeceUNA");
        FlowController.getInstance().goViewInWindow("LogIn");
     }

    public static void main(String[] args) {
        launch();
    }
    
}
